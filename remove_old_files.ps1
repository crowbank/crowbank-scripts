<#
A script to remove older files.

Files older than 2 days are removed from \\NAS\SQL and most \\NAS\Homes\Reolink
subfolders, except \\NAS\Homes\Reolink\Kitchen and \\NAS\Homes\Reolink\Cattery
where files older than 7 days are removed.

#>

$folders = @(
    @{ Path = "\\NAS\Homes\Reolink\Front"; Days = 2},
    @{ Path = "\\NAS\Homes\Reolink\Field"; Days = 2},
    @{ Path = "\\NAS\Homes\Reolink\North"; Days = 2},
    @{ Path = "\\NAS\Homes\Reolink\South"; Days = 2},
    @{ Path = "\\NAS\Homes\Reolink\Road"; Days = 2},
    @{ Path = "\\NAS\Homes\Reolink\Kitchen"; Days = 7},
    @{ Path = "\\NAS\Homes\Reolink\Cattery"; Days = 7},
    @{ Path = "\\NAS\SQL"; Days = 3}
)

$folders | Foreach-Object {
    Push-Location $_.Path
    $days = $_.Days
    $files = (Get-ChildItem -Recurse . | Where-Object {
        !$_.PSIsContainer -and $_.LastWriteTime -lt (Get-Date).AddDays(-$days)})

    $files | ForEach-Object { Remove-Item $_.FullName }

    $dirs = Get-ChildItem -Recurse -Directory . | Where-Object {
        (Get-ChildItem $_.FullName).Count -eq 0 } | Select-Object -ExpandProperty FullName
    
    Write-Host $dirs
    $dirs | ForEach-Object { Remove-Item $_ }
        <#

    do {
        $dirs = Get-ChildItem -Recurse -Directory . | Where-Object {
            (Get-ChildItem $_.FullName).Count -eq 0 } | Select-Object -ExpandProperty FullName
        $dirs | ForEach-Object { Remove-Item $_ }
    } while ($dirs.Count -gt 0)
    #>

    Pop-Location
}
