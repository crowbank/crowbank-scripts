Function Get-CrowbankVersion {
    Write-Output "2020-10-16 (1)"
}
Function Invoke-CrowbankSql {
    param(
        [Parameter(HelpMessage="SQL Command",
        Mandatory = $true,
        Position = 0)]
        [string] $Command,
        [Parameter(HelpMessage="Database to use")]
        [string] $Database = "crowbank",
        [Parameter(HelpMessage="Execute stored procedure and return the return value")]
        [switch] $RV
    )

    if ($null -eq $Env:Dev -or $Env:Dev -ne 1) {
        $ServerInstance = "HP-SERVER\SQLEXPRESS"
    } else {
        $ServerInstance = "HOUSE\SQLEXPRESS"
    }

    $Username = "sa"
    $Password = "Crowbank454"

    if ($RV) {
        $RV_Command = $Command.Replace("'", "''")
        return (Invoke-Sqlcmd -ServerInstance "$ServerInstance" -Database "$Database" -Username "$Username" -Password "$Password" "exec pexecute_with_return_value '$RV_Command'").rv
    } else {
        Invoke-Sqlcmd -ServerInstance "$ServerInstance" -Database "$Database" -Username "$Username" -Password "$Password" "$Command"
    }
}

<# Function Invoke-CrowbankMysql {
    param(
        [Parameter(HelpMessage="SQL Command",
        Mandatory = $true,
        Position = 0)]
        [string] $Command,
        [Parameter(HelpMessage="Use Development Environment")]
        [Switch]
        [Alias("dev", "D")]
        $Development,
        [Parameter(HelpMessage="Database to use")]
        [string] $Database = "crowbank_petadmin"
    )

    if ($Development) {
        $ServerInstance = "localhost"
    } else {
        $ServerInstance = "HP-SERVER"
    }

    $MYSQLPASSWORD = "Crowbank454"
    $MYSQLUSER = "crowbank"
    

}
 #>
function my ($cmd) {
    mysql "-p$MYSQLPASSWORD" "-u$MYSQLUSER" -e "$cmd" --skip-column-names -s 2> $null
}

Function ConvertTo-String ( $DateInput ) {
    If ([string]::IsNullOrWhiteSpace($DateInput)) {
        return "NULL"
    }

    [datetime]$date = $DateInput
    return "'$($date.ToString('yyyy-MM-dd'))'"
}

Function Read-Date {
    param (
        [string]$Prompt,
        [datetime]$MinDate,
        [string]$StartValue,
        [Switch]$AllowNull
    )

    $date = New-Object DateTime

    do {
        if ([string]::IsNullOrWhiteSpace($StartValue)) {
            $response = Read-Host $Prompt
        } else {
            $response = $StartValue
        }

        $StartValue = ""
        if ([string]::IsNullOrWhiteSpace($response)) {
            if ($AllowNull) {
                return
            }
            Write-Host "A valid date must be entered."
            continue
        }

        if ($response -match "(\d+)/(\d+)") {
            $day = $Matches[1]
            $month = $Matches[2]
            $year = (Get-Date).Year
            $date = Get-Date -Year $year -Month $month -Day $day
            if ($null -ne $MinDate -and $date -lt $MinDate) {
                $date = $date.AddYears(1)
            }
            return $date
        } elseif ([DateTime]::TryParse($response, [Ref]$date)) {
            if ($null -eq $MinDate -or $date -ge $MinDate) {
                return $date
            } else {
                Write-Host ("Date must be on or after {0}" -f $MinDate.ToString("dd/MM/yyyy"))
            }
        } else {
            Write-Host "Enter date in valid format such as yyyy-mm-dd, or simply dd/mm"
        }
    } while ($true)
}

Function Read-Amount {
    param (
        [Parameter(Mandatory=$true)]
        [string]$Prompt,
        [Switch]$AllowNull
    )

    [decimal]$amount = 0.0

    Do {
        $response = Read-Host $Prompt
        If ([string]::IsNullOrWhiteSpace($response)) {
            If ($AllowNull) {
                Return
            }
            Write-Host "A valid amount must be entered."
            Continue
        }
        If ([decimal]::TryParse($response, [ref]$amount)) {
            return $amount
        }
        Write-Host "Please enter an amount"
    } While ($true)
}
Function Test-Confirmation
{
    [CmdletBinding(SupportsShouldProcess)]
    Param(
        [Parameter(ValueFromPipeline=$true)]
        $item
    )
    process {
        if ($PSCmdlet.ShouldProcess("$item")) {
            Write-Host "Moving Ahead"
        } else {
            Write-Host "Ok, will stop"
        }
    }
}
Function Test-ValidDate
{
    param(
        [string]$string
    )

    $string -ne '' -and $string.GetType().FullName -ne 'DBNull'
}



class HRTask
{
    [int]$EmployeeNo
    [string]$EmployeeNickname
    [string]$TaskName
    [string]$TaskCode
    [string]$TaskType
    [string]$TaskDescription
    [datetime]$AssignedDate
    [string]$Status
    [datetime]$CompleteDate
    [int]$IsCurrent

    HRTask([System.Data.DataRow]$row) {
        $this.EmployeeNo = $row.et_emp_no
        $this.EmployeeNickname = $row.et_emp_nickname
        $this.TaskName = $row.et_task_name
        $this.TaskCode = $row.et_task_code
        $this.TaskType = $row.et_task_type
        $this.TaskDescription = $row.et_task_desc
        $this.IsCurrent = $row.et_iscurrent
        $this.Status = $row.et_status
        if ($row.et_assign_date -is [datetime]) {
            $this.AssignedDate = $row.et_assign_date
        }

        if ($row.et_complete_date -is [datetime]) {
            $this.CompleteDate = $row.et_complete_date
        }

    }

    [string] ToString() {
        return $this.EmployeeNickname + ': ' + $this.TaskCode
    }
}

Function Get-HRTask {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Employee Nickname")]
        [Alias("Name", "Nickname", "Employee")]
        [string] $EmployeeNickname,
        [Parameter(HelpMessage="Get all tasks, including completed ones")]
        [Switch]
        [Alias("A")]
        $All,
        [Parameter(HelpMessage="List available tasks")]
        [Switch]
        $List
    )
    begin {
        if ($List) {
            Invoke-CrowbankSql "select task_code, task_name, task_desc from tblhrtask where task_live = 1" | format-table
        } else {
            $cmd = "Select * from vwemployeetask"
            $tasks = Invoke-CrowbankSql $cmd
            $out = @()
        }
    }
    process {
        if (-not $List) {
            $out += (
                $tasks | Where-Object {
                    $task = $_
                    if (-not $All -and $task.et_iscurrent -eq 0) { $false }
                    elseif ($EmployeeNickname -ne '') { $task.et_emp_nickname -eq $EmployeeNickname }
                    elseif ($null -eq $item) { $true }
                    else {
                        Switch($item.GetType().FullName ) {
                            "System.String" {
                                $task.et_emp_nickname -Match $item -or
                                $task.et_task_name -Match $item -or
                                $task.et_task_code -Match $item -or
                                $task.et_status -Match $item -or
                                $task.et_task_type -Match $item
                            }
                            "System.Int32" {
                                $task.et_emp_no -eq $item    
                            }
                            "Employee" {
                                $task.et_emp_no -eq $item.EmployeeNo
                            }
                        }
                    }    
                } | ForEach-Object { [HRTask]::new($_) }
            )
        }
    }
    end {
        if (-not $List) {
            $out | Sort-Object -Property AssignedDate
        }
    }
}

Function New-HRTask
{
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Nickname",
        Mandatory=$true)]
        [string]$Nickname,
        [Parameter(HelpMessage="Task Code")]
        [Alias("Task")]
        [string]$TaskCode,
        [Parameter(HelpMessage="Assigned Date")]
        [Alias("Assigned")]
        [datetime]$AssignedDate
    )
    New-Start
    $sql = "Select count(*) c from vwemployeetask where et_emp_nickname = '$Nickname' and et_task_code = '$TaskCode'"
    $c = (Invoke-CrowbankSql $sql).c

    If ($c -gt 0) {
        throw "Employee nicknamed $Nickname was already assigned task $TaskCode"
    }

    $sql = @"
pcreate_task '$Nickname', '$TaskCode', $(ConvertTo-String($AssignedDate))
"@

    Write-Verbose $sql
    $rv = Invoke-CrowbankSql $sql -RV
    if ($rv -ne 0) {
        New-Error "$sql Failed"
        return $rv
    }
    New-LogEntry "Created new task code $TaskCode for $Nickname"
    
    Get-HRTask -EmployeeNickname $Nickname $TaskCode 
}

Function Set-HRTask
{
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Nickname",
        Mandatory=$true)]
        [string]$Nickname,
        [Parameter(HelpMessage="Task Code",
        Mandatory=$true)]
        [Alias("Task")]
        [string]$TaskCode,
        [Parameter(HelpMessage="Complete Date")]
        [Alias("Complete")]
        [datetime]$CompleteDate
    )

    $sql = @"
Exec pcomplete_hr_task '$Nickname', '$TaskCode', $(ConvertTo-String($CompleteDate))
"@

    Write-Verbose $sql
    Invoke-CrowbankSql $sql
}

class Employee
{
    [int]$EmployeeNo
    [string]$Surname
    [string]$Forename
    [string]$Nickname
    [string]$Addr1
    [string]$Addr3
    [string]$Postcode
    [string]$Mobile
    [string]$Emergency
    [string]$TelnoHome
    [string]$Email
    [string]$Facebook
    [string]$Rank
    [datetime]$StartDate
    [datetime]$EndDate
    [datetime]$DOB
    [decimal]$CurrentRate
    [bool]$IsCurrent
    [int]$Order
    
    Employee([System.Data.DataRow]$row) {
        $this.EmployeeNo = $row.emp_no
        $this.Surname = $row.emp_surname
        $this.Forename = $row.emp_forename
        $this.Nickname = $row.emp_nickname
        $this.Addr1 = $row.emp_addr1
        $this.Addr3 = $row.emp_addr3
        $this.Postcode = $row.emp_postcode
        $this.Mobile = $row.emp_telno_mobile
        $this.Emergency = $row.emp_telno_emergency
        $this.TelnoHome = $row.emp_telno_home
        $this.Email = $row.emp_email
        $this.Facebook = $row.emp_facebook
        $this.DOB = $row.emp_dob
        $this.IsCurrent = $row.emp_iscurrent
        If (![string]::IsNullOrWhiteSpace($row.emp_current_rate)) {
            $this.CurrentRate = $row.emp_current_rate
        }
        if ($row.emp_start_date -is [datetime]) {
            $this.StartDate = $row.emp_start_date
        }
        if ($row.emp_end_date -is [datetime]) {
            $this.EndDate = $row.emp_end_date
        }
        if ($row.emp_order -is [int]) {
            $this.Order = $row.emp_order
        } else {
            $this.Order = 0
        }
        
        $this.Rank = $row.emp_rank
    }

    [string] ToString() {
        return $this.Nickname
    }
}

Function New-Employee {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Nickname (must be unique)",
        Mandatory=$true)]
        [string]$Nickname,
        [Parameter(HelpMessage="First Name",
        Mandatory=$true)]
        [string]$Forename,
        [Parameter(HelpMessage="Last Name",
        Mandatory=$true)]
        [string]$Surname,
        [Parameter(HelpMessage="Email Address")]
        [string]$Email,
        [Parameter(HelpMessage="Facebook Address")]
        [string]$Facebook,
        [Parameter(HelpMessage="Street Address")]
        [Alias("Street", "Addr1")]
        [string]$StreetAddress,
        [Parameter(HelpMessage="City")]
        [Alias("City", "Town", "Addr3")]
        [string]$CityAddress,
        [Parameter(HelpMessage="Postcode")]
        [string]$Postcode,
        [Parameter(HelpMessage="Mobile Number")]
        [Alias("Telno_Mobile", "Telno")]
        [string]$Mobile,
        [Parameter(HelpMessage="Emergency Number")]
        [Alias("Telno_Emergency")]
        [string]$Emergency,
        [Parameter(HelpMessage="Home Number")]
        [Alias("Telno_Home", "HomeNumber")]
        [string]$TelnoHome,
        [Parameter(HelpMessage="Start Date")]
        [Alias("Start")]
        [datetime]$StartDate,
        [Parameter(HelpMessage="National Insurance Number")]
        [string]$NI,
        [Parameter(HelpMessage="Date of Birth")]
        [datetime]$DOB,
        [Parameter(HelpMessage="Rank of Employee")]
        [ValidateSet("Manager", "Shift Leader", "Kennel Assistant", "Dog Walker", "Volunteer",
        "M", "S", "K", "D", "V")]
        [string]$Rank,
        [Parameter(HelpMessage="Current Rate")]
        [Alias("Rate")]
        [decimal]$CurrentRate
    )
    New-Start
    $sql = "Select count(*) c from tblemployee where emp_nickname = '$Nickname'"
    $c = (Invoke-CrowbankSql $sql).c

    If ($c -gt 0) {
        throw "Employee nicknamed $Nickname already exists"
    }

    If ([string]::IsNullOrWhiteSpace($Email)) { $Email = Read-Host "Email" }
    If ([string]::IsNullOrWhiteSpace($Facebook)) { $Facebook = Read-Host "Facebook Address" }
    If ([string]::IsNullOrWhiteSpace($StreetAddress)) { $StreetAddress = Read-Host "Street Address" }
    If ([string]::IsNullOrWhiteSpace($CityAddress)) { $CityAddress = Read-Host "City" }
    If ([string]::IsNullOrWhiteSpace($Postcode)) { $Postcode = Read-Host "Postcode" }
    If ([string]::IsNullOrWhiteSpace($Mobile)) { $Mobile = Read-Host "Mobile Number" }
    If ([string]::IsNullOrWhiteSpace($Emergency)) { $Emergency = Read-Host "Emergency Number" }
    If ([string]::IsNullOrWhiteSpace($TelnoHome)) { $TelnoHome = Read-Host "Home Number" }
    If ([string]::IsNullOrWhiteSpace($StartDate)) { $StartDate = Read-Date "Start Date" }
    If ([string]::IsNullOrWhiteSpace($NI)) { $NI = Read-Host "National Insurance Number" }
    If ([string]::IsNullOrWhiteSpace($DOB)) { $DOB = Read-Date -AllowNull "Date of Birth" }
    $RankNumber = 'NULL'
    Do {
        If ([string]::IsNullOrWhiteSpace($Rank)) {
            $Rank = Read-Host "Rank ([M]anager, [S]hift Leader, [K]ennel Assistant, [D]og Walker or [V]olunteer)"
            If ([string]::IsNullOrWhiteSpace($Rank)) {
                Continue
            }
        }
        $sql = "Select min(er_no) er_no from tblemployeerank where er_desc like '$Rank%'"
        Write-Verbose $sql
        $RankNumber = (Invoke-CrowbankSql $sql).er_no
        If ($null -eq $RankNumber) {
            Write-Host "Unknown Rank"
            $Rank = ''
        }
    } While ( $null -eq $RankNumber)

    If ($CurrentRate -eq 0.0) { $CurrentRate = Read-Amount -AllowNull "Current Rate" }
    If ($StartDate -gt (Get-Date)) {
        $IsCurrent = 0
    } else {
        $IsCurrent = 1
    }

    If ($CurrentRate -gt 0.0) {
        $RateString = $CurrentRate.ToString()
    } else {
        $RateString = "NULL"
    }

    $sql = @"
Insert into tblemployee
(emp_nickname, emp_forename, emp_surname, emp_email, emp_facebook, emp_addr1, emp_addr3, emp_postcode,
emp_telno_mobile, emp_telno_emergency, emp_telno_home, emp_start_date, emp_ni, emp_dob, emp_current_rate,
emp_iscurrent, emp_rank_no) values (
    '$Nickname', '$Forename', '$Surname', '$Email', '$Facebook', '$StreetAddress', '$CityAddress', '$Postcode',
    '$Mobile', '$Emergency', '$TelnoHome', $(ConvertTo-String($StartDate)), '$NI', $(ConvertTo-String($DOB)),
    $RateString, $IsCurrent, $RankNumber)
"@

    Write-Verbose $sql
    Invoke-CrowbankSql $sql

    If ($CurrentRate -gt 0.0) {
        $sql = "pupdate_employee_rate '$Nickname', $(ConvertTo-String($StartDate)), $CurrentRate"
        Write-Verbose $sql
        $rv = Invoke-CrowbankSql $sql -RV
        if ($rv -ne 0) {
            New-Error "$sql Failed"
            return $rv
        }
    }

    if ($CurrentRate -gt 0.0) {
        $sql = "Select emp_no from tblemployee where emp_nickname = '$Nickname'"
        Write-Verbose $sql
        [int]$Number = (Invoke-CrowbankSql $sql).emp_no
    }
    Get-Employee -EmployeeNo $Number -All
}
Function Get-Employee {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Employee Number")]
        [Alias("No", "Number", "EmpNumber")]
        [ValidateRange(1,999999)]
        [int] $EmployeeNo,
        [Parameter(HelpMessage="Get all employees, including non-current ones")]
        [Switch]
        [Alias("A")]
        $All
    )
    begin {
        $cmd = "Select * from vwemployee"
        $emps = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $emps | Where-Object {
            $emp = $_
                if (-not $All -and $emp.emp_current_or_future -eq 0) { $false }
                elseif ($EmployeeNo -gt 0) { $emp.emp_no -eq $EmployeeNo }
                elseif ($null -eq $item) { $true }
                else {
                    Switch($item.GetType().FullName ) {
                        "System.String" {
                            $emp.emp_forename -Match $item -or
                            $emp.emp_surname -Match $item -or
                            $emp.emp_nickname -Match $item -or
                            $emp.emp_addr1 -Match $item -or
                            $emp.emp_addr3 -Match $item -or
                            $emp.emp_postcode -Match $item -or
                            $emp.emp_telno_mobile -Match $item -or
                            $emp.emp_email -Match $item -or
                            $emp.emp_facebook -Match $item -or
                            $emp.emp_telno_emergency -Match $item -or
                            $emp.emp_telno_home -Match $item -or
                            $emp.emp_rank -Match $item
                        }
                        "System.Int32" {
                            $emp.emp_no -eq $item    
                        }
                    }
                }    
            } | ForEach-Object { [Employee]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property Nickname -Unique
    }
}


class Vet
{
    [int]$VetNo
    [string]$PracticeName
    [string]$Addr1
    [string]$Addr3
    [string]$Postcode
    [string]$Telno1
    [string]$Email
    [string]$Website

    Vet([System.Data.DataRow]$row) {
        $this.VetNo = $row.vet_no
        $this.PracticeName = $row.vet_practice_name
        $this.Addr1 = $row.vet_addr1
        $this.Addr3 = $row.vet_addr3
        $this.Postcode = $row.vet_postcode
        $this.Telno1 = $row.vet_telno_1
        $this.Email = $row.vet_email
        $this.Website = $row.vet_website
    }

    [string]ToString() {
        return ("{0}: {1}" -f $this.VetNo, $this.PracticeName)
    }
}

Function Get-Vet {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Vet Number")]
        [Alias("No", "Number", "VetNumber")]
        [ValidateRange(1,999999)]
        [int] $VetNo
    )
    begin {
        $cmd = "Select * from vwvet_mysql"
        $vets = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $vets | Where-Object {
                $vet = $_
                if ($VetNo -gt 0) { $vet.vet_no -eq $VetNo }
                elseif ($null -eq $item) { $true }
                else {
                    Switch($item.GetType().FullName ) {
                        "System.String" {
                            $vet.vet_practice_name -Match $item -or
                            $vet.vet_addr1 -Match $item -or
                            $vet.vet_addr3 -Match $item -or
                            $vet.vet_postcode -Match $item -or
                            $vet.vet_telno_1 -Match $item -or
                            $vet.vet_email -Match $item -or
                            $vet.vet_website -Match $item    
                        }
                        "System.Int32" {
                            $vet.vet_no -eq $item    
                        }
                        "Pet" {
                            $vet.vet_no -eq $item.VetNo    
                        }
                    }
                }    
            } | ForEach-Object { [Vet]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property VetNo -Unique
    }
}

class Breed
{
    [int]$BreedNo
    [string]$Species
    [string]$Description
    [string]$ShortDescription
    [string]$BillCategory

    Breed([System.Data.DataRow]$row) {
        $this.BreedNo = $row.breed_no
        if ($row.breed_spec_no -eq 1 ) {
            $this.Species = 'Dog'
        } else {
            $this.Species = 'Cat'
        }
        $this.Description = $row.breed_desc
        $this.ShortDescription = $row.breed_shortdesc
        $this.BillCategory = switch($row.breed_billcat_no) {
            -1 { "Misc" }
            3 { "Dog large" }
            5 { "Dog medium" }
            7 { "Dog small" }
            11 { "Cat" }
        }
    }

    [string]ToString(){
        return ("{0}: {1}" -f $this.BreedNo, $this.Description)
    }
}

Function Get-Breed {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Breed Number")]
        [Alias("No", "Number", "BreedNumber")]
        [ValidateRange(1,999999)]
        [int] $BreedNo
    )
    begin {
        $out = @()
        $cmd = "Select * from vwbreed_mysql"
        $breeds = Invoke-CrowbankSql $cmd
    }
    process {
        $out += (
            $breeds | Where-Object {
                $b = $_
                if ($BreedNo -gt 0) { $_.breed_no -eq $BreedNo}
                elseif ($null -eq $item ) { $true }
                else { 
                    Switch ( $item.GetType().FullName ) {
                        "System.Int32" { $b.breed_no -eq $item }
                        "System.String" {
                            $b.breed_desc -Match $item -or
                            $b.breed_shortdesc -Match $item
                        }
                        "Pet" {
                            $b.breed_no -eq $item.BreedNo
                        }
                    }
                }
            } | ForEach-Object { [Breed]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property BreedNo -Unique
    }
}

class Customer
{
    [int]$CustomerNo
    [string]$Title
    [string]$Surname
    [string]$Forename
    [string]$Addr1
    [string]$Addr3
    [string]$Postcode
    [string]$TelnoHome
    [string]$TelnoMobile
    [string]$TelnoMobile2
    [string]$Email
    [string]$Email2
    [bool]$NoDeposit
    [string]$PetDesc
    [int[]]$PetNos
    [string[]]$PetNames
    [int]$PastBookings

    Customer([System.Data.DataRow]$row) {
        $this.CustomerNo = $row.cust_no
        $this.Title = $row.cust_title
        $this.Surname = $row.cust_surname
        $this.Forename = $row.cust_forename
        $this.Addr1 = $row.cust_addr1
        $this.Addr3 = $row.cust_addr3
        $this.Postcode = $row.cust_postcode
        $this.TelnoHome = $row.cust_telno_home
        $this.TelnoMobile = $row.cust_telno_mobile
        $this.TelnoMobile2 = $row.cust_telno_mobile2
        $this.Email = $row.cust_email
        $this.Email2 = $row.cust_email2
        $this.NoDeposit = $row.cust_nodeposit
        $this.PetNos = $row.cust_petnos.Split(",")
        $this.PetNames = $row.cust_petnames.Split(",")
        $this.PetDesc = $row.cust_petdesc
        $this.PastBookings = $row.cust_past_bookings
    }

    [string]ToString(){
        $full_name = $this.Surname
        if ($this.Forename -ne '') {
            $full_name = $this.Forename + ' ' + $this.Surname
        }
        if ($this.Title -ne '') {
            $full_name = $this.Title + ' ' + $full_name
        }
        return ("{0}: {1} [{2}]" -f $this.CustomerNo, $full_name, $this.Email)
    }

}

Function Get-Customer {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Search by customer number, name, address, phone number, etc.", ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Customer Number")]
        [Alias("No", "Number", "CustomerNumber")]
        [ValidateRange(1,999999)]
        [int] $CustomerNo
    )
    begin {
        $cmd = "Select * from vwcustomer_ps"
        $customers = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $customers | Where-Object {
                $c = $_
                if ( $CustomerNo -gt 0 ) {
                    $CustomerNo -eq $_.cust_no 
                }
                elseif ( $null -eq $item ) {
                    $true
                } else {
                    Switch ($item.GetType().FullName) {
                        "System.Int32" {
                            $item -eq $c.cust_no
                        }
                        "System.String" {
                            $c.cust_surname -Match $item -or
                            $c.cust_forename -Match $item -or
                            $c.cust_addr1 -Match $item -or
                            $c.cust_addr3 -Match $item -or
                            $c.cust_postcode -Match $item -or
                            $c.cust_email -Match $item -or
                            $c.cust_email2 -Match $item            
                        }
                        "Pet" {
                            $c.cust_no -eq $item.CustomerNo 
                        }
                        "Booking" {
                            $c.cust_no -eq $item.CustomerNo
                        }
                        "Request" {
                            $c.cust_no -eq $item.CustomerNo
                        }
                    }
                }
            } | ForEach-Object { [Customer]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property CustomerNo -Unique
    }
}


class Pet
{
    [int]$PetNo
    [int]$CustomerNo
    [string]$Surname
    [string]$Name
    [string]$Species
    [int]$BreedNo
    [string]$BreedDescription
    [datetime]$DOB
    [string]$Warning
    [char]$Sex
    [char]$Neutered
    [int]$VetNo
    [string]$VaccinationStatus
    [datetime]$VaccinationDate
    [bool]$Deceased
    [datetime]$KCDate
    [string]$Notes
    [string]$VaccinationPath

    [string]ToString(){
        return ("{0}: {1} {2} ({3})" -f $this.PetNo, $this.Name, $this.Surname, $this.BreedDescription)
    }

    Pet([System.Data.DataRow]$row) {
        $this.PetNo = $row.pet_no
        $this.CustomerNo = $row.pet_cust_no
        $this.Surname = $row.cust_surname

        $this.Name = $row.pet_name
        $this.BreedNo = $row.pet_breed_no
        $this.BreedDescription = $row.breed_shortdesc
        if ($row.pet_spec_no -eq 1) {
            $this.Species = 'Dog'
        } else {
            $this.Species = 'Cat'
        }

        if (Test-ValidDate $row.pet_dob) { $this.DOB = $row.pet_dob }
        $this.Warning = $row.pet_warning
        $this.Sex = $row.pet_sex
        $this.Neutered = $row.pet_neutered
        $this.VetNo = $row.pet_vet_no

        $this.VaccinationStatus = $row.pet_vacc_status
        if (Test-ValidDate $row.pet_vacc_date ) { $this.VaccinationDate = $row.pet_vacc_date }
        $this.Deceased = ($row.pet_deceased -eq 'Y')
        if (Test-ValidDate $row.pet_kc_date ) { $this.KCDate = $row.pet_kc_date }
        $this.Notes = $row.pet_notes
        $this.VaccinationPath = $row.vacc_path
    }
}

Function Get-Pet {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Search by pet number, name and breed", ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Pet Number")]
        [Alias("No", "Number", "PetNumber")]
        [ValidateRange(1,999999)]
        [int] $PetNo,
        [Parameter(HelpMessage="Checked-in pets only")]
        [switch]
        [Alias("C", "In", "I")]
        $CheckedIn
    )
    begin {
        $cmd = "Select * from vwpet_mysql
join vwcustomer_mysql on cust_no = pet_cust_no
join vwbreed_mysql on breed_no = pet_breed_no
left join vwbookingitem on bi_pet_no = pet_no and checked_in = 1"
        $pets = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $pets | Where-Object {
                $p = $_
                if ($PetNo -gt 0) {
                    $p.pet_no -eq $PetNo 
                }
                elseif ($null -eq $item) {
                    $true
                } else {
                    Switch ($item.GetType().FullName ) {
                        "System.Int32" {
                            $item -eq $p.pet_no
                        }
                        "System.String" {
                            $p.pet_name -Match $item -or
                            $p.cust_surname -Match $item -or
                            $p.cust_addr1 -Match $item -or
                            $p.cust_addr3 -Match $item -or
                            $p.cust_postcode -Match $item -or
                            $p.cust_email -Match $item -or
                            $p.cust_email2 -Match $item -or
                            $p.breed_desc -Match $item -or
                            $p.breed_shortdesc -Match $item
                        }
                        "Customer" {
                            $item.PetNos.Contains($p.pet_no)
                        }
                        "Booking" {
                            $item.PetNos.Contains($p.pet_no)
                        }
                        "Breed" {
                            $p.breed_no -eq $item.BreedNo 
                        }
                    }
                }
            } | Where-Object {
                (-not $CheckedIn) -or ('' -ne $_.checked_in)
            } |
            ForEach-Object { [Pet]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property PetNo -Unique
    }
}

class Booking
{
    [int]$BookingNo
    [int]$CustomerNo
    [datetime]$StartDate
    [datetime]$EndDate
    [string]$StartTime
    [string]$EndTime
    [decimal]$GrossAmount
    [decimal]$PaidAmount
    [string]$Notes
    [string]$Memo
    [char]$Status
    [datetime]$CreateDate
    [bool]$Deluxe
    [string]$PetNames
    [string]$CustomerSurname
    [string]$CustomerEmail
    [string]$PayLink
    [decimal]$Deposit
    [datetime]$ConfirmationDate
    [string]$ConfirmationEmail
    [string]$ConfirmationSubject
    [string]$ConfirmationFile

    [int[]]$PetNos

    Booking([System.Data.DataRow]$row) {
        $this.BookingNo = $row.bk_no
        $this.CustomerNo = $row.bk_cust_no
        $this.CustomerSurname = $row.cust_surname
        $this.CustomerEmail = $row.cust_email

        $this.StartDate = $row.bk_start_date
        $this.StartTime = $row.bk_start_time
        $this.EndDate = $row.bk_end_date
        $this.EndTime = $row.bk_end_time
        $this.GrossAmount = $row.bk_gross_amt
        $this.PaidAmount = $row.bk_paid_amt
        $this.Notes = $row.bk_notes
        $this.Memo = $row.bk_memo
        $this.Status = $row.bk_status
        $this.CreateDate = $row.bk_create_date
        $this.Deluxe = $row.bk_deluxe

        $this.PetNos = $row.bk_petnos.Split(",")
        $this.PetNames = $row.bk_petnames

        $this.PayLink = $row.bk_pay_link
        $this.Deposit = $row.bk_deposit

        if ('' -ne $row.hist_date) {
            $this.ConfirmationDate = $row.hist_date
        }
        
        $this.ConfirmationEmail = $row.hist_destination
        $this.ConfirmationFile = $row.hist_msg
        $this.ConfirmationSubject = $row.hist_subject
    }

    [string]ToString(){
        return ("{0}: {1} [{2} - {3}]" -f $this.BookingNo, $this.PetNames, $this.StartDate, $this.EndDate)
    }

    [void]SendConfirmation($Force) {
        $s = (python C:\Crowbank\generate_confirmation.py $($this.BookingNo))
        $a = $s.Split("`t")
        $filename = $a[0]
        $filename_txt = $a[1]
        $subject = $a[2]
        $path = "K:/Confirmations"

        If ($Force) {
            python C:/Crowbank/send_confirmation.py $this.CustomerEmail, $subject, $filename, $filename_txt, $this.Deposit, $this.BookingNo
            return
        }

        Invoke-Item "$path/$filename"


        $prompt = 'Send confirmation [Y]es or [N]o?'
        $yes = New-Object System.Management.Automation.Host.ChoiceDescription '&Yes', 'Sends the confirmation'
        $no = New-Object System.Management.Automation.Host.ChoiceDescription '&No', 'Does NOT send the confirmation'

        $options = [System.Management.Automation.Host.ChoiceDescription[]] ($yes, $no)

        $choice = (Get-Host).ui.PromptForChoice('', $prompt, $options, 0)

        if ($choice -eq 0) {
            python C:/Crowbank/send_confirmation.py $this.CustomerEmail, $subject, $filename, $filename_txt, $this.Deposit, $this.BookingNo
        }
    }
}

function Send-Confirmation {
    [CmdletBinding(SupportsShouldProcess=$true,
    ConfirmImpact="High")]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Send confirmation without review")]
        [Switch]
        $Force
    )
    process {
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $booking = Get-Booking -BookingNo $item
            }
            "Booking" {
                $booking = $item
            }
        }
        $booking.SendConfirmation($Force)
    }
}

function Get-Booking {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Booking Number")]
        [Alias("No", "Number", "BookingNumber")]
        [int] $BookingNo,
        [Parameter(HelpMessage="Time: P (past), C (current), F (future) or any combination, e.g. CF for current or future")]
        [Alias("T")]
        [string] $Time = "CF",
        [Parameter(HelpMessage="Booking Status: B (regular), V (confirmed), C (cancelled), P (provisional), S (standby), N (no-show)")]
        [string] $Status = "BVO",
        [Parameter(HelpMessage="Force refresh of staging table; use just after making changes; will make retrieval slower")]
        [Switch]
        $Force,
        [Parameter(HelpMessage="Bookings active on the date")]
        [datetime] $On
    )
    Begin {
        If ($force) {
            Write-Verbose "Running refresh_ps"
            Invoke-CrowbankSql "Execute refresh_ps"
        }
        If ($BookingNo -gt 0) {
            $abort = $true
            $cmd = "Select * from vw_ps_booking where bk_no = $BookingNo"
            $row = Invoke-CrowbankSql $cmd
            Return [Booking]::new($row)
        }
        $cmd = "Select * from vw_ps_booking"
        $abort = $false
        $bookings = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        if (-not $abort) {
            $out += (
                $bookings | Where-Object {
                    $b = $_
                    if ($null -eq $item) {
                        $true
                    }
                    else {
                        Switch($item.GetType().FullName) {
                            "System.Int32" {
                                $b.bk_no -eq $item
                            }
                            "System.String" {
                                $b.cust_surname -Match $item -or
                                $b.cust_addr1 -Match $item -or
                                $b.cust_addr3 -Match $item -or
                                $b.cust_postcode -Match $item -or
                                $b.cust_email -Match $item -or
                                $b.cust_email2 -Match $item -or
                                $b.bk_petnames -match $item
                            }
                            "Customer" {
                                $b.bk_cust_no -eq $item.CustomerNo
                            }
                            "Pet" {
                                $b.bk_petnos.Split(",").Contains([string]$item.PetNo)
                            }
                            "Request" {
                                $b.bk_no -eq $item.BookingNo
                            }
                        }
                    }
                } | Where-Object {
                    ($Time -Match "P" -and $_.bk_end_date -le (Get-Date)) -or
                    ($Time -Match "C" -and $_.bk_end_date -ge (Get-Date).AddDays(-1) -and $_.bk_start_date -le (Get-Date)) -or
                    ($Time -Match "F" -and $_.bk_start_date -ge (Get-Date).AddDays(-1)) -or
                    ($time -eq 'All')
                } | Where-Object {
                    ($Status -Match "B" -and $_.bk_status -eq ' ') -or
                    $Status -Match $_.bk_status
                } | Where-Object {
                    $null -eq $On -or ($_.bk_start_date -le $On -and $_.bk_end_date -ge $On )
                } | ForEach-Object { [Booking]::new($_) }
            )
        }
    }
    end {
        $out | Sort-Object -Property BookingNo -Unique
    }
}

Function New-Booking {
    [CmdletBinding(SupportsShouldProcess = $true,
    ConfirmImpact = 'High')]
    param (
        [Parameter(HelpMessage="Customer for which booking is to be made",
        ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Customer for which booking is to be made",Position=0)]
        $Customer,
        [Parameter(HelpMessage="Start Date", Position=1)]
        [string]$StartDate,
        [Parameter(HelpMessage="Start Time [am or pm]", Position=2)]
        [ValidateSet("am", "pm")]
        [string]$StartTime = "am",
        [Parameter(HelpMessage="End Date", Position=3)]
        [string]$EndDate,
        [Parameter(HelpMessage="End Time [am or pm]", Position=4)]
        [ValidateSet("am", "pm")]
        [string]$EndTime = "am",
        [Parameter(HelpMessage="Pets: list names or use 'all' (default)", Position=5)]
        [string[]]$Pets = "all",
        [Parameter(HelpMessage="Is Deluxe Booking")]
        [switch]$Deluxe,
        [Parameter(HelpMessage="Comment or booking note")]
        [string]$Comment,
        [Parameter(HelpMessage="Status ([O]nline (default), [B]ooking, [V]erified, [P]rovisional or [S]tandby; Online bookings auto-expire unless confirmed")]
        [ValidateSet("O", "B", "V", "P", "S")]
        [string]$Status = "O",
        [Parameter(HelpMessage="All pets are allocated seperate runs")]
        [switch]$Seperate,
        [Parameter(HelpMessage="Make a hold bookings (status 'O')")]
        [Alias("B", "Book")]
        [switch]$Hold,
        [Parameter(HelpMessage="Override safety checks (e.g. Sunday afternoon arrival/departure)")]
        [switch]$Force,
        [Parameter(HelpMessage="Number of booking request form submission to use")]
        [Alias("F")]
        [int]$Form=0,
        [Parameter(HelpMessage="Book as trial, i.e. no charge, single day, 14:00-16:00")]
        [Alias("T")]
        [switch]$Trial,
        [Parameter(HelpMessage="Origin of the command. Leave blank when called manually")]
        [string]$Origin = 'New-Booking'
    )

    process {
        New-Start
        
        if ($null -ne $Form -and $Form -gt 0) {
            $sql = "Select count(*) c from tblformdata_6 where fd_no = $Form"
            if ((Invoke-CrowbankSql $sql).c -eq 0) {
                New-Error "Unable to find form $Form" $Form 'F'
                return
            }
            $sql = "Select fd_cust_no cust_no from tblformdata_6 where fd_no = $Form"
            $cust_no = (Invoke-CrowbankSql $sql).cust_no
            if ($null -eq $cust_no) {
                New-Error "Form $Form has no valid customer" $Form 'B'
                return
            }
            $sql = "select max(log_no) log_no from tbllog"
            $log_no = (Invoke-CrowbankSql $sql).log_no

            if ($Deluxe) {
                $dlx_int = 1
            } else {
                $dlx_int = 0
            }
            $sql = "exec pcreate_booking_form $Form, 1, '$Status', $dlx_int, '$Origin'"
            $bk_no = (Invoke-CrowbankSql $sql).bk_no
            if ($null -eq $bk_no -or '' -eq $bk_no -or 0 -eq $bk_no) {
                New-Error "$sql Failed"
            } else {
                Write-Output "Booking #$bk_no created"
            }


            $sql = "Select log_no, log_msg, log_sev_desc, log_context from vwlog where log_no > $log_no"
            Invoke-CrowbankSql $sql | ForEach-Object {
                $sev = $_.log_sev_desc
                if ($sev -eq 'INFO') {
                    Write-Verbose "$($_.log_no): $($_.log_context) - $($_.log_msg)"
                } elseif ($sev -eq 'WARNING') {
                    Write-Warning "$($_.log_no): $($_.log_context) - $($_.log_msg)"
                } elseif ($sev -eq 'ERROR') {
                    Write-Error "$($_.log_no): $($_.log_context) - $($_.log_msg)"
                } elseif ($sev -eq 'DEBUG') {
                    Write-Debug "$($_.log_no): $($_.log_context) - $($_.log_msg)"
                }
            }
            return
        }

        If ($Hold) {
            $Status = 'O'
        }

        $StartDate_ = [datetime]::Parse($StartDate)

        if ($null -eq $item) {
            $item = $Customer
        }

        If ($item -is [Customer]) {
            $Customer = $item
        } else {
            $Customer = Get-Customer $item
            Write-Verbose "Found $($Customer.Count) Customers"
            If ($Customer.Count -eq 0) {
                New-Warning "Unable to find customer"
                Return
            } elseif ($Customer.Count -gt 10) {
                New-Warning "Search item $item matches more than ten customers"
                Return
            } elseif ($Customer.Count -gt 1) {
                [int]$i
                $Candidates = $Customer | Select-Object CustomerNo, Surname, PetDesc, PastBookings |
                Sort-Object -Property PastBookings -Descending |
                ForEach-Object { $i = 1 } { $_ | Add-Member -MemberType NoteProperty -Name Rank -Value $i; $i = $i + 1; $_ } |
                    Select-Object Rank, CustomerNo, Surname, PetDesc, PastBookings

                $CandidateCount = $Customer.Count
                Do {
                    Write-Host "The following $CandidateCount customers where found."
                    $Candidates | Format-Table

                    $input = Read-Host "Please enter customer rank [1 - $CandidateCount] or 0 to quit (default = 1)"
                    If ($input -eq '') {
                        $input = 1
                    }
                    [int]$n = $input
                } Until ($n -le $CandidateCount -and $n -ge 0)

                If ($n -eq 0) {
                    Return
                }
                $Customer = $Candidates | Where-Object { $_.Rank -eq $n }
                $Customer = Get-Customer $Customer.CustomerNo
            }
        }

        If ($Pets -eq "all") {
            $Pets_ = Get-Pet $Customer | Where-Object { -not $_.Deceased }
            if ($Trial) {
                $Pets_ = $Pets_ | Where-Object { $_.Species -eq 'Dog' }
            }
        } else {
            $Pets_ = $Pets | Get-Pet | Where-Object { $_.CustomerNo -eq $Customer.CustomerNo -and -not $_.Deceased }
        }

        If ($Pets_.Count -eq 0) {
            New-Error "With Pets=$Pets, none found"
            Return
        }

        $PetNames = ($Pets_ | ForEach-Object { $_.Name }) -Join ", "

        $PetNumbers = ($Pets_ | ForEach-Object { $_.PetNo }) -Join ","

        if ($null -eq $StartDate_) {
            $StartDate_ = Read-Date "Start Date" -MinDate (Get-Date) -StartValue $StartDate
        }

        if ($Seperate) {
            $Sharing = 0
        } else {
            $Sharing = 1
        }

        if ($Trial) {
            $sql = "Select max(log_no) log_no from tbllog"
            $LogNo = (Invoke-CrowbankSql $sql).log_no
            $sql =
@"
Execute pcreate_trial_booking $($Customer.CustomerNo), $(ConvertTo-String($StartDate_)), '$PetNumbers', '$Comment', $Sharing, '$Origin'
"@
            Write-Verbose $sql

            [int]$bk_no = (Invoke-CrowbankSql $sql).bk_no

            If ($bk_no) {
                Write-Output "Booking #$bk_no created successfully"
            } else {
                Write-Output "No booking was created"
            }
            $sql = "Select log_sev_desc, log_msg from vwlog where log_no > $LogNo and log_context = 'pcreate_trial_booking' and log_sev_no > 1"
            Write-Verbose $sql
            Invoke-CrowbankSql $sql | ForEach-Object { Write-Output "$($_.log_sev_desc): $($_.log_msg)"}
            return
        }

        If ($StartDate_.DayOfWeek -eq 'Sunday') {
            If ($StartTime -eq "pm" -and -not $Force) {
                $StartTime = "am"
                New-Warning "Sunday start - changing arrival to morning"
            }
        } elseif ($null -eq $StartTime) {
            Do {
                $StartTime = Read-Host "Start Time ([am] (default) or [pm])"
                If ($null -eq $StartTime) {
                    $StartTime = "am"
                }
                If ($StartTime -eq "am" -or $StartTime -eq "pm") {
                    Continue
                }
            } While ($true)
        }


        $EndDate_ = [datetime]::Parse($EndDate)

        if ($null -eq $EndDate_) {
            $EndDate_ = Read-Date "End Date" -MinDate $StartDate -StartValue $EndDate
        }

        If ($EndDate_.DayOfWeek -eq "Sunday") {
            If ($EndTime -eq "pm" -and -not $Force) {
                $EndTime = "am"
                New-Warning "Sunday end - changing departure to morning"
            }
        } elseif ($null -eq $EndTime) {
            Do {
                $EndTime = Read-Host "End Time ([am] (default) or [pm])"
                If ($null -eq $EndTime) {
                    $EndTime = "am"
                }
                If ($EndTime -eq "am" -or $EndTime -eq "pm") {
                    Continue
                }
            } While ($true)
        }
        
        If ($Deluxe) {
            $IsDeluxe = 1
        } else {
            $IsDeluxe = 0
        }

        $sql =
@"
Select isnull(min(bk_no), 0) bk_no from pa..tblbooking
where bk_cust_no = $($Customer.CustomerNo)
and bk_start_date < $(ConvertTo-String($EndDate_))
and bk_end_date > $(ConvertTo-String($StartDate_))
"@

        Write-Verbose $sql
        [int]$bk_no = (Invoke-CrowbankSql $sql).bk_no

        If ($bk_no -gt 0) {
            New-Warning "Found an overlapping booking #$bk_no" $bk_no 'B'
            If (-not $Force) {
                Return
            }
        }
        $msg =
@"
Create booking (status $Status) for customer $($Customer.Surname) #$($Customer.CustomerNo)
for $PetNames from $($StartDate_.ToString('dd/MM/yyyy')) ($StartTime) to $($EndDate_.ToString('dd/MM/yyyy')) ($EndTime)
"@
        if ($pscmdlet.ShouldProcess($msg)) {
            $sql = "Select max(log_no) log_no from tbllog"
            $LogNo = (Invoke-CrowbankSql $sql).log_no

            $sql =
@"
Execute pcreate_online_booking $($Customer.CustomerNo), $(ConvertTo-String($StartDate_)), $(ConvertTo-String($EndDate_)),
'$StartTime', '$EndTime', '$PetNumbers', '$Status', '$Comment', $IsDeluxe, $Sharing, 0, '$Origin'
"@
            Write-Verbose $sql
            [int]$bk_no = (Invoke-CrowbankSql $sql).bk_no

            If ($bk_no) {
                Write-Output "Booking #$bk_no created successfully"
            } else {
                Write-Output "No booking was created"
            }
            $sql = "Select log_sev_desc, log_msg from vwlog where log_no > $LogNo and log_context = 'pcreate_online_booking' and log_sev_no > 1"
            Invoke-CrowbankSql $sql | ForEach-Object { Write-Output "$($_.log_sev_desc): $($_.log_msg)"}
        }
    }
}

Function New-PayLink {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Provide a booking, a booking or a voucher number",
        ValueFromPipeline=$true, Position=0, Mandatory=$true)]
        $item,
        [Parameter(HelpMessage="Amount to charge [default=outstanding amount]")]
        [float]$Amount = 0.0,
        [Parameter(HelpMessage="Open URL Immediately")]
        [Switch]
        [Alias("O")]
        $Open,
        [Parameter(HelpMessage="Voucher")]
        [Switch]
        [Alias("V")]
        $Voucher
    )
    process {
        New-Start
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $bk_no = $item
                if ($Voucher) {
                    $bk_no = -$bk_no
                } else {
                    $booking = Get-Booking -BookingNo $bk_no
                }
            }
            "Booking" {
                $bk_no = $item.BookingNo
                $booking = $item
            }
        }

        if (-not $Voucher -and $null -eq $booking) {
            New-Error "Could not load booking #$bk_no" $bk_no 'B'
            return
        }

        if ($Amount -gt 0.0 -or $Voucher) {
            $sql = "Select dbo.fn_payment_link($bk_no, $Amount) link"
            $link = (Invoke-CrowbankSql $sql).link
        } elseif ($null -eq $booking.PayLink) {
            New-Error "The booking has no link" $bk_no 'B'
            return
        } else {
            $link = $booking.PayLink
        }

        if ($Open) {
            Start-Process $link
        } else {
            Write-Output $link
        }
    }
}


function New-VetBill {
    [CmdletBinding(SupportsShouldProcess = $true,
    ConfirmImpact = 'High')]
    param (
        [Parameter(HelpMessage="Pet or Pet Number", Mandatory=$true)]
        [Alias("PetNo", "Pet", "No", "N")]
        $item,
        [Parameter(HelpMessage="Amount to Charge", Mandatory=$true)]
        [decimal]
        $Amount,
        [Parameter(HelpMessage="Visit Date (defaults to today's date)")]
        [Alias("Date", "D", "On")]
        [datetime]$VisitDate,
        [Parameter(HelpMessage="Use yesterday as payment date")]
        [Switch]
        [Alias("Y")]
        $Yesterday,
        [Parameter(HelpMessage="Number of trips to charge (defaults to 1)")]
        [int]$Visits = 1
    )
    begin {
        New-Start
        if ($null -eq $VisitDate) {
            $VisitDate = (Get-Date)
            if ($Yesterday) {
                $VisitDate = $VisitDate.AddDays(-1)
            }
        }
        if ($null -eq $Visits) {
            $Visits = 1
        }
    }
    process {
        $pet_name = ''
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $pet_no = $item
            }
            "Pet" {
                $pet_no = $item.PetNo
            }
            "System.String" {
                $pet_name = $item
                $count = (Invoke-CrowbankSql "Select count(*) c from vwbookingitem where checked_in = 1 and bi_pet_name = '$pet_name'").c
                if ($count -eq 1) {
                    $pet_no = (Invoke-CrowbankSql "Select bi_pet_no from vwbookingitem where bi_pet_name = '$pet_name' and checked_in = 1").bi_pet_no
                } else {
                    if ($count -eq 0) {
                        New-Error "Unable to find checked-in pet named $pet_name" $pet_no 'P'
                    } else {
                        New-Error "More than one checked-in pet named $pet_name" $pet_no 'P'
                    }
                    return
                }
            }
        }
        if ($pet_name -eq '') {
            $pet_name = (Invoke-CrowbankSql "Select pet_name from pa..tblpet where pet_no = $pet_no").pet_name
        }
        Write-Verbose "Pet: $pet_name #$pet_no ($Visits visits)"
        $breed = (Invoke-CrowbankSql "Select breed_desc from vwpet where pet_no = $pet_no").breed_desc
        $msg = "Adding vet bill of £$Amount"
        if ($Visits -eq 0 -or $null -eq $Visits) {
            $msg = $msg + " (no transport charge)"
        } elseif ($Visits -gt 1) {
            $msg = $msg + " (charging $Visits visits)"
        }
        $msg = $msg + " for $pet_name ($breed) vet visit on $VisitDate"
        if ($pscmdlet.ShouldProcess($msg)) {
            $cmd = "Execute padd_bill $pet_no, '$VisitDate', $Amount, $Visits"
            Write-Verbose $cmd
            Invoke-CrowbankSql $cmd
        }
    }
}
Function New-Payment {
    [CmdletBinding(SupportsShouldProcess = $true,
    ConfirmImpact = 'High')]
    param (
        [Parameter(HelpMessage="Provide a booking or a booking number",
        ValueFromPipeline=$true, Position=0, Mandatory=$true)]
        $item,
        [Parameter(HelpMessage="Payment Method (one of Pay By Link, Payzone, Cash or Bank Xfer)")]
        [ValidateSet("Pay by Link", "Payzone", "Cash", "Bank Xfer")]
        [Alias("PT", "Type")]
        [string]$PaymentType = "Pay by Link",
        [Parameter(HelpMessage="Payment Date (defaults to today's date)")]
        [Alias("Date", "D", "On")]
        [datetime]$PaymentDate,
        [Parameter(HelpMessage="Use yesterday as payment date")]
        [Switch]
        [Alias("Y")]
        $Yesterday,
        [Switch]
        $Force,
        [Parameter(HelpMessage="Payment Amount (defaults to requested deposit)")]
        [Alias("A")]
        [decimal]$Amount
    )
    begin {
        New-Start
        If ($null -eq $PaymentDate) {
            $PaymentDate = (Get-Date)
            If ($Yesterday) {
                $PaymentDate = $PaymentDate.AddDays(-1)
            }
        }
        Write-Verbose "Processing payment for date $PaymentDate"
    }
    process {
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $bk_no = $item
                $booking = Get-Booking -BookingNo $bk_no
                Write-Verbose "Using booking #$bk_no"
            }
            "Booking" {
                $bk_no = $item.BookingNo
                $booking = $item
            }
        }

        If ($null -eq $booking) {
            $msg = "Could not load booking #$bk_no"
            New-Error $msg $bk_no 'B'
            Return
        }

        If ($booking.PaidAmount -gt 0.0 -and -not $Force) {
            $msg = "A payment has already been made. To register another, used -Force"
            New-Warning $msg $bk_no 'B'
            Return $booking
        }

        If ($null -eq $Amount -or $Amount -eq 0.0) {
            $Amount = (Invoke-CrowbankSql "Select * from tbldepositrequest where dr_bk_no = $bk_no").dr_amount
            If ($null -eq $Amount -or $Amount -eq 0.0) {
                $msg = "Unable to find an amount for booking #$bk_no"
                New-Error $msg $bk_no 'B'
                Return
            }
        }

        $msg = "Payment of £$Amount for $booking"
        If ($pscmdlet.ShouldProcess($msg)) {
            $cmd = "ppayment $bk_no, $Amount, '$PaymentType', '$PaymentDate'"
            Write-Verbose $cmd
            $rv = Invoke-CrowbankSql $cmd -RV
            if ($rv -ne 0) {
                Write-Error "$sql Failed"
                return $rv
            }
        }
    }
}

class Request
{
    [int]$RequestNo
    [int]$CustomerNo
    [datetime]$CreatedDate
    [datetime]$StartDate
    [datetime]$EndDate
    [string]$Comments
    [string]$Pets
    [string]$Status
    [string[]]$Issues
    [int]$BookingNo
    [string]$Period

    Request([System.Data.DataRow]$row) {
        $this.RequestNo = $row.fd_no
        $this.CustomerNo = $row.fd_cust_no
        $this.CreatedDate = $row.fd_date_created
        $this.StartDate = $row.fd_bk_start_date
        $this.EndDate = $row.fd_bk_end_date
        $this.Comments = $row.fd_comments
        $this.Pets = $row.fd_pets
        $this.Status = $row.fd_status

        $issue_string = $row.fd_issues
        if ($null -ne $issue_string) {
            $this.Issues = $issue_string.Split('/')
        }
        
        $this.BookingNo = $row.fd_bk_no
        $this.Period = $row.fd_period
    }

    [string]ToString(){
        return ("{0}: {1} [{2} - {3}]" -f $this.RequestNo, $this.PetNames, $this.StartDate, $this.EndDate)
    }
}

Function Get-Request {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Search by request number, customer or status", ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Request Number")]
        [Alias("No", "Number", "fd_no")]
        [ValidateRange(1,999999)]
        [int] $RequestNo,
        [Parameter(HelpMessage="Search for past as well as future requests")]
        [switch]$All
    )
    begin {
        $dead_states = 'moot', 'cancelled', 'rejected'
        $cmd = "Select fd_no, fd_email, fd_surname, isnull(fd_cust_no, 0) fd_cust_no, fd_date_created, fd_bk_start_date, fd_bk_end_date, fd_comments, fd_pets, fd_status, isnull(fd_issues, '') fd_issues, isnull(fd_bk_no, 0) fd_bk_no, fd_period from vwformdata_6"
        $requests = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $requests | Where-Object {
                $r = $_
                if ($RequestNo -gt 0) {
                    $r.fd_no -eq $RequestNo 
                }
                elseif ($null -eq $item) {
                    $true
                } else {
                    Switch ($item.GetType().FullName ) {
                        "System.Int32" {
                            $item -eq $r.fd_no
                        }
                        "System.String" {
                            $r.fd_email -Match $item -or
                            $r.fd_surname -Match $item -or
                            $r.fd_status -Match $item
                        }
                        "Customer" {
                            $item.CustomerNo -eq $r.fd_cust_no
                        }
                    }
                }
            } | Where-Object {
                $All -or (-Not ($_.fd_status -In $dead_states) -And $_.fd_bk_start_date -gt (Get-Date))
            } |
            ForEach-Object { [Request]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property RequestNo -Unique
    }

}

Function Read-Amazon {
    <#
.SYNOPSIS

Read an Amazon Orders (Crowbank custom) report into the database.
#>
    [CmdletBinding()]
    param (
            [Parameter(HelpMessage="Name of Amazon report file", Mandatory=$false)]
            [string] $path
    )
    process {
        New-Start
        if ('' -eq $path) {
            $path = (New-Object -ComObject Shell.Application).NameSpace('shell:Downloads').Self.Path + "\orders.txt"
            Write-Verbose "No path was specified. Using the default $path instead"
        }
        $temp = "orders.txt"
        if (Test-Path $path) {
            Get-Content $path |
            Select-Object -Skip 1 |
            Where-Object {
                $a = $_.Split("`t")
                $a[9] -ne "N/A"
            } |
            ForEach-Object {
                "$_`n"
            } |
            Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII

            Invoke-CrowbankSql "truncate table tblamazonitem_staging"
            bcp tblamazonitem_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            $rv = Invoke-CrowbankSql "pimport_amazon_items" -RV
            if ($rv -ne 0) {
                Write-Error "$sql Failed"
                return $rv
            }
        } else {
            New-Error "Unable to find file $path"
        }
    }
}

Function Read-AmazonNew {
    <#
.SYNOPSIS

Read an Amazon Orders (Crowbank custom) report into the database.
#>
    [CmdletBinding()]
    param (
            [Parameter(HelpMessage="Name of Amazon report file", Mandatory=$false)]
            [string] $path
    )
    process {
        Write-Verbose "Read-Amazon $path"
        if ('' -eq $path) {
            $path = (New-Object -ComObject Shell.Application).NameSpace('shell:Downloads').Self.Path + "\orders.csv"
            Write-Verbose "No path was specified. Using the default $path instead"
        }
        $temp = "Z:/Accounts/amazon/orders.txt"
        $temp1 = "Z:/Accounts/amazon/orders1.txt"
        if (Test-Path $path) {
            Import-CSV $path | Export-CSV -Delimiter "`t" -Path $temp1
        
            Get-Content $temp1 |
            Select-Object -Skip 1 |
            Where-Object {
                $a = $_.Split("`t")
                $a[9] -ne "N/A"
            } |
            ForEach-Object {
                "$_`n"
            } |
            Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII

            Invoke-CrowbankSql "truncate table tblamazonitem_staging"
            bcp tblamazonitem_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            $rv = Invoke-CrowbankSql "pimport_amazon_items" -RV
            if ($rv -ne 0) {
                New-Error "pimport_amazon_items Failed"
            }
        } else {
            New-Error "Unable to find file $path"
        }
    }
}

Function Read-Clockdata {
    <#
.SYNOPSIS

Read an attendence clock log data file into the database.
#>
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Name of Clock data file")]
        [string] $path
    )
    process {
        $temp = "./logdata.txt"
        if ($null -eq $path) {
            $path = GLogData_.txt
        }
        if (Test-Path $path) {
            (Get-Content $path) | Select-Object -Skip 1 | ForEach-Object  -Begin { $h = @{} }  -Process {
                $a = $_.Split("`t")
                $d = $a[6].Substring(0, 10) -Replace "/", "-"
                $t = $a[6].Substring(12, 5)
                [int]$n = $a[2]
                $e = $a[3].Trim()
                $k = $e + $d
                $h.Item($k) += 1
                "{0}`t{1}`t{2}`t{3}`t{4}`n" -f $d, $t, $n, $e, $h.Item($k)
            } | Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII
            Invoke-CrowbankSql "truncate table tblclockdata_staging"
            bcp tblclockdata_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            Invoke-CrowbankSql "exec ppopulate_clockdata"
        } else {
            New-Error "Unable to find or open {0}" -f $path
        }
    }
}


function Read-WorldpayOld {
    <#
.SYNOPSIS

Read a Worldpay Capture report into the database.
#>
    param (
        [Parameter(HelpMessage="Name of Worldpay report file", Mandatory=$true)]
        [string] $path
    )
    begin {
        New-Start
        $start = $false
        $temp = "Z:\Accounts\WorldPay Reports\wp.txt"
    }
    process {
        if (Test-Path $path) {
            Get-Content $path | ForEach-Object -Process {
                if ($start) {
                    $a = $_.Split("`t")
                    $mer = $a[0]
                    $tid = $a[1]
                    $cart = $a[2]
                    $customer = $a[3]
                    $type = $a[9]
                    $date = $a[11]
                    $amount = $a[13]
                    $m = $cart -match "PBL-(\d+)"
                    if ($m) {
                        $bk_no = $Matches[1]
                        "{0}`t{1}`t{2}`t{3}`t{4}`t{5}`t{6}`n" -f $mer, $tid, $customer, $type, $date, $amount, $bk_no
                    }
                } elseif ($_ -match "Transaction ID") {
                    $start = $true
                }
            } | Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII
            Invoke-CrowbankSql "truncate table tblworldpay_staging"
            bcp tblworldpay_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            $rv = Invoke-CrowbankSql "pimport_worldpay" -RV
            if ($rv -ne 0) {
                New-Error "pimport_worldpay Failed"
                return $rv
            }
        } else {
            New-Error "Unable to find or open {0}" -f $path
        }
    }
}

function Read-Worldpay {
    <#
.SYNOPSIS

Read a Worldpay Capture report into the database.
#>
    param (
        [Parameter(HelpMessage="Name of Worldpay report file", Mandatory=$true)]
        [string] $path
    )
    begin {
        New-Start
        $start = $false
        $temp = "C:\Users\the_y\Dropbox\Accounts\WorldPay Reports\wp.txt"
    }
    process {
        if (Test-Path $path) {
            Get-Content $path | ForEach-Object -Process {
                if ($start) {
                    $a = $_.Split("`t")
                    $tid = $a[0]
                    $cart = $a[1]
                    $customer = $a[2]
                    $date = $a[3]
                    $amount = $a[4]
                    $type = $a[5]
                    if ($tid -match 'Event*') {
                        $start = $false
                    }
                    else {
                        $m1 = $cart -match "PBL-(\d+)"
                        $m2 = $cart -match "PBL-V(\d+)"
                        $m3 = $cart -match "VT-.*-(\d+)"

                        $cart_key = 0
                        if ($m1 -or $m2 -or $m3) {
                            $cart_key = $Matches.1
                        }
                        if ($m2) {
                            $cart_type = 'V'
                        } else {
                            $cart_type = 'B'
                        }
                    # if ($m) {
                        "{0}`t{1}`t{2}`t{3}`t{4}`t{5}`t{6}`t{7}`n" -f $tid, $cart, $customer, $date, $amount, $type, $cart_key, $cart_type
                    # }
                    }
                } elseif ($_ -match "Transaction ID") {
                    $start = $true
                }
            } | Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII
            Invoke-CrowbankSql "truncate table tblworldpay_staging"
            bcp tblworldpay_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            $rv = Invoke-CrowbankSql "pimport_worldpay" -RV
            if ($rv -ne 0) {
                New-Error "pimport_worldpay Failed"
                return $rv
            }
        } else {
            New-Error "Unable to find or open {0}" -f $path
        }
    }
}

$IntranetPwd = '26b43d6cf4a9c7b25a1ab38b6153987c'

Function Restart-Intranet {
    & 'C:\Program Files (x86)\AlwaysUp\AlwaysUp.exe' -restart Intranet
}
Function Start-Intranet {
    Invoke-WebRequest "http://hp-server:8585/api/restart?password=$IntranetPwd&application=Intranet" -UseBasicParsing
}

Function Stop-Intranet {
    Invoke-WebRequest "http://hp-server:8585/api/stop?password=$IntranetPwd&application=Intranet" -UseBasicParsing
}

Function Get-IntranetStatus {
    (Invoke-WebRequest "http://hp-server:8585/api/get-status?password=$IntranetPwd&application=Intranet" -UseBasicParsing).Content | Select-Xml -Xpath "//state" | Select-Object -ExpandProperty  "node"
}

Function Copy-StaticItems {
    robocopy 'K:/Vet Bills' 'C:\Crowbank\crowbank-flask\intranet\static\vetbills' *
    robocopy 'K:/Instructions' 'C:\Crowbank\crowbank-flask\intranet\static\instructions' *
    robocopy 'K:/Medication Sheets' 'C:\Crowbank\crowbank-flask\intranet\static\meds' *
    robocopy 'K:/Photos' 'C:\Crowbank\crowbank-flask\intranet\static\photos' *
    robocopy 'K:/Run Cards' 'C:\Crowbank\crowbank-flask\intranet\static\run_cards' *
    robocopy 'K:/Vaccinations' 'C:\Crowbank\crowbank-flask\intranet\static\vaccinations' *
}

Function Install-Crowbank {
    <#
.SYNOPSIS

Copy Petadmin-Module from development folder (if exists) to both Z:\Website\Crowbank\Petadmin-Module and to C:\Crowbank\Petadmin-Module.
If the development folder does not exist, the file is copied from the Z to the C drive
#>
    param (
        [Parameter(HelpMessage="Pull latest pytadmin version")]
        [switch] $Pytadmin,
        [Parameter(HelpMessage="Pull crowbank-python files")]
        [switch] $Python,
        [Parameter(HelpMessage="Update Crowbank Intranet")]
        [switch] $Intranet,
        [Parameter(HelpMessage="Update Crowbank Scripts")]
        [switch] $Scripts,
        [Parameter(HelpMessage="Update Crowbank Intranet, pytadmin and python files")]
        [switch] $All
    )

    $dev_path = "C:\dev\crowbank-scripts\Petadmin-Module\Petadmin-Module.psm1"
    $z_path = "\\NAS\dropbox\Website\Crowbank\Petadmin-Module\Petadmin-Module.psm1"
    $c_path = "C:\Crowbank\Petadmin-Module\Petadmin-Module.psm1"
    $script_path = "C:\Crowbank\crowbank-scripts"
    $pytadmin_path = "C:\Program Files\Python310\Lib\site-packages\pytadmin"
    $dev_path = "C:\Users\the_y\dev\crowbank-scripts\Petadmin-Module\Petadmin-Module.psm1"
    $z_path = "\\NAS\dropbox\Website\Crowbank\Petadmin-Module\Petadmin-Module.psm1"
    $c_path = "C:\Crowbank\Petadmin-Module\Petadmin-Module.psm1"
    $script_path = "C:\Crowbank\crowbank-scripts"
    $pytadmin_path = "C:\Program Files\Python10\Lib\site-packages\pytadmin"
    $python_package = "\\NAS\dropbox\Website\crowbank-python"
    $flask_package = "\\NAS\dropbox\Website\crowbank-flask"
    
    if (Test-Path $dev_path) {
        Copy-Item $dev_path $z_path
    }

    Copy-Item $z_path $c_path

    if ($Scripts -or $All) {
        Push-Location $script_path
        git pull
        Pop-Location
    }

    if ($Pytadmin -or $All) {
        Push-Location $pytadmin_path
        git pull
        Pop-Location
    }

    if ($Python -or $All) {
        Push-Location $python_package
        git pull
        Copy-Item *.py C:/Crowbank
        Copy-Item *.html C:/Crowbank
        Pop-Location
    }

    if ($Intranet -or $All) {
        Push-Location $flask_package
        git pull
        Stop-Intranet
        robocopy . c:/crowbank/crowbank-flask /S /XD .git /XJ /XJD /XD icons /XD external
        Start-Intranet
        Pop-Location
    }    

    Write-Output 'im $m -Force'
}

Function Send-CrowbankEmail {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Address to which email is sent", Mandatory=$true)]
        [string] $to,        
        [Parameter(HelpMessage="Subject of the message", Mandatory=$true)]
        [string] $subject,
        [Parameter(HelpMessage="Body of the message", Mandatory=$false)]
        [string] $body
    )
    process {
        $secpasswd = ConvertTo-SecureString "-SlQ-!FXnLnl" -AsPlainText -Force
        $cred = New-Object System.Management.Automation.PSCredential ("info@crowbank.co.uk", $secpasswd)
        Send-MailMessage -To $to -Subject $subject -Body $body -SmtpServer "cp165172.hpdns.net" -From "info@crowbank.co.uk" -Credential $cred
    }
}

Function Get-PaymentMessage {
    <#
.SYNOPSIS

Retrieve email messages relating to Worldpay payments, and attempt to process them as necessary
#>
[CmdletBinding()]
    param (
        [Parameter(HelpMessage="Force reading all messages as of this date/time")]
        [string]$CutoffDate
    )
    process {
        New-Start
        $file = "C:/Crowbank/ExportedGmailPassword.txt"
        $user = "crowbank.partners@gmail.com"

        $cred = New-Object -TypeName System.Management.Automation.PSCredential `
        -ArgumentList $user, (Get-Content $file | ConvertTo-SecureString)

        $gmail = New-GmailSession -Credential $cred
        $mailbox = Get-Mailbox -Session $gmail -Label "Worldpay"

        if ($null -eq $CutoffDate) {
            Write-Verbose "Importing all unread messages"
            $messages = $mailbox | Get-Message -Session $gmail -Unread -Prefetch
        } else {
            [datetime]$cutoff = [datetime]::Parse($CutoffDate)
            Write-Verbose "Importing all messages since $cutoff"
            $messages = $mailbox | Get-Message -Session $gmail -After $cutoff -Prefetch
        }

        $mc = $messages.Count
        Write-Verbose "Downloaded $mc messages"
        Foreach ($message in $messages) {
            $subject = $_.Subject
            Write-Verbose "Reading message $subject"
            $body = $_.Body.Split("`n")
            $bk_no = 0
            $amount = 0.0

            Foreach ($line in $body) {
                If ($line -match "PBL-(\d+)") {
                    [int]$bk_no = $Matches[1]
                }
                If ($line -match "Sale value:\s*GBP\s*(\d+\.\d+)") {
                    [float]$amount = $Matches[1]
                }
            }

            $booking = Get-Booking -BookingNo $bk_no
            $expected_amount = (Invoke-CrowbankSql "Select * from tbldepositrequest where dr_bk_no = $bk_no").dr_amount

            If ($booking.PaidAmount -gt 0.0 -or $expected_amount -ne $amount ) {
                New-Error "Already paid, or amounts not matching" $bk_no 'B'
            } Else  {
                $date = $message.Date
                $cmd = "ppayment $bk_no, $amount, 'Pay by Link', '$date'"
                $rv = Invoke-CrowbankSql $cmd -RV
                if ($rv -ne 0) {
                    New-Error "$sql Failed"
                } else {
                    New-LogEntry "Added payment of $amount to booking #$bk_no" 'info' $bk_no 'B'
                }
            }
            Update-Message -Message $_ -Session $gmail -Read
        }
    }
}

Function New-LogEntry {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Message to be included in the log entry")]
        [Alias("Msg", "M")]
        [string]$Message,
        [Parameter(HelpMessage="Severity Level (debug/info/warning/error/fatal")]
        [Alias("Sev")]
        [ValidateSet("debug", "info", "warning", "error", "critical")]
        [string]$Severity="info",
        [Parameter(HelpMessage="Key of relevant object (e.g. booking or form)")]
        [int]$Key=0,
        [Parameter(HelpMessage="Type of object (e.g. B or F)")]
        [string]$KeyType,
        [Parameter(HelpMessage="How deep is this call")]
        [int]$Depth=0
    )
    process {
        $msg = $Message.Replace("'", "''")
        $s = (Get-PSCallStack)[1+$Depth]
        $loc = $s.Location
        $com = $s.Command
        $line = 0
        $file = ''
        if ($loc -match "^(.*):\s*line (\d+)") {
            $line = $Matches[2]
            $file = $Matches[1]
        }

        $sql = "exec plog '$msg', '$Severity', '$com', $Key, '$KeyType', '$file', $line"
        Invoke-CrowbankSql $sql
    }
}

Function New-Error {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Message to be included in the log entry")]
        [Alias("Msg", "M")]
        [string]$Message,
        [Parameter(HelpMessage="Key of relevant object (e.g. booking or form)")]
        [int]$Key=0,
        [Parameter(HelpMessage="Type of object (e.g. B or F)")]
        [string]$KeyType
    )
    process {
        Write-Error $Message
        New-LogEntry $Message 'error' $Key $KeyType -Depth 1
    }
}

Function New-Warning {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Message to be included in the log entry")]
        [Alias("Msg", "M")]
        [string]$Message,
        [Parameter(HelpMessage="Key of relevant object (e.g. booking or form)")]
        [int]$Key=0,
        [Parameter(HelpMessage="Type of object (e.g. B or F)")]
        [string]$KeyType
    )
    process {
        Write-Warning $Message
        New-LogEntry $Message 'warning' $Key $KeyType -Depth 1
    }
}

Function New-Start {
    [CmdletBinding()]
    param (
    )
    process {
        $s = (Get-PSCallStack)[-2]
        $com = $s.Command
        $arg = $s.Arguments

        $msg = "Starting $com $arg"

        Write-Verbose $msg
        New-LogEntry $msg 'info' -Depth 1
    }
}

Function Enter-HPServer {
    Enter-PSSession HP-SERVER
}

Function Send-Notice {
    <#
.SYNOPSIS

Send emails to customers using specific views
#>
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Type of notice (default: all)")]
        [Alias("N")]
        [string]$Notice='all',
        [Parameter(HelpMessage="Booking Number (optional)")]
        [Alias("Booking", "B")]
        [int[]]$BookingNo,
        [Parameter(HelpMessage="Max number of notices to display (per view)")]
        [Alias("M", "Max")]
        [int]$MaxNotices=3,
        [Parameter(HelpMessage="Display (email if not used)")]
        [switch]$Display
    )
    begin {
        New-Start
    }
    process {
        $disp = ''
        if ($Display) {
            $disp = '-display'
        }

        $bookings = ''
        if ($BookingNo) {
            $bookings = '-booking ' + $BookingNo
        }
        $cmd = "python C:/Crowbank/email_customers.py -view $Notice $bookings -max $MaxNotices $disp"
        Invoke-Expression $cmd
    }

}

Function Invoke-Daily {
    <#
.SYNOPSIS

Run daily tasks such as emailing customers ahead of arrival
#>
    New-Start
#    Read-FG
    python C:/Crowbank/crowbank-flask/scripts/email_customers.py -scheduled daily
    python C:/Crowbank/crowbank-flask/scripts/sms.py
#    python C:/Crowbank/distance.py
    Invoke-CrowbankSql "execute ppurge_expired_bookings"
    Invoke-CrowbankSql "execute pmaintenance"
}

Function Invoke-Hourly {
    <#
.SYNOPSIS

Run a number of maintenance tasks, including retrieving Worldpay and Request emails
#>
    New-Start
    python C:/Crowbank/process_worldpay.py
#    python C:/Crowbank/process_requests.py
    python C:/Crowbank/crowbank-flask/scripts/email_customers.py -scheduled hourly
    Copy-StaticItems
#    python C:/Crowbank/retrieve_requests.py
}

Function Read-Clockdata2 {
    <#
.SYNOPSIS

Read an attendence clock log data file into the database.
#>
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Name of Clock data file")]
        [string] $path
    )
    process {
        $temp = "./logdata.txt"
        if ($null -eq $path) {
            $path = GLogData_.txt
        }
        if (Test-Path $path) {
            (Get-Content $path) | Select-Object -Skip 1 | ForEach-Object  -Begin { $h = @{} }  -Process {
                $a = $_.Split("`t")
                $d = $a[6].Substring(0, 10) -Replace "/", "-"
                $t = $a[6].Substring(12, 5)
                [int]$n = $a[2]
                $e = $a[3].Trim()
                $k = $e + $d
                $h.Item($k) += 1
                "{0}`t{1}`t{2}`t{3}`t{4}`n" -f $d, $t, $n, $e, $h.Item($k)
            } | Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII
            Invoke-CrowbankSql "truncate table tblclockdata_staging"
            bcp tblclockdata_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            Invoke-CrowbankSql "exec ppopulate_clockdata"
        } else {
            New-Error "Unable to find or open {0}" -f $path
        }
    }
}

Function Read-Clockdata-Orig {
    <#
.SYNOPSIS

Read an attendence clock log data file into the database.
#>
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Name of Clock data file")]
        [string] $path
    )
    process {
        $temp = "Z:/Employment/Clock Data/logdata.txt"
        if ($null -eq $path) {
            $path = GLogData_.txt
        }
        if (Test-Path $path) {
            (Get-Content $path) | Select-Object -Skip 1 | ForEach-Object  -Begin { $h = @{} }  -Process {
                $a = $_.Split("`t")
                $d = $a[6].Substring(0, 10) -Replace "/", "-"
                $t = $a[6].Substring(12, 5)
                [int]$n = $a[2]
                $e = $a[3].Trim()
                $k = $e + $d
                $h.Item($k) += 1
                "{0}`t{1}`t{2}`t{3}`t{4}`n" -f $d, $t, $n, $e, $h.Item($k)
            } | Out-File -Force -FilePath $temp -NoNewline -Encoding ASCII
            Invoke-CrowbankSql "truncate table tblclockdata_staging"
            bcp tblclockdata_staging in $temp -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
            Invoke-CrowbankSql "exec ppopulate_clockdata"
        } else {
            New-Error "Unable to find or open {0}" -f $path
        }
    }
}
Function Read-ClockData {
    (Get-Content GLogData_.txt) | Select-Object -Skip 1 | ForEach-Object -Process {
        $a = $_.Split("`t")
        $d = $a[6].Substring(0, 10) -Replace "/", "-"
        $t = $a[6].Substring(12, 5)
        [int]$n = $a[2]
        $e = $a[3].Trim()
        $k = $e + $d
        $h.Item($k) += 1
        "{0}`t{1}`t{2}`t{3}`t{4}`n" -f $d, $t, $n, $e, $h.Item($k)
    } | Out-File -FilePath logdata.txt -NoNewline -Encoding ASCII
    Invoke-CrowbankSql "truncate table tblclockdata_staging"
    bcp tblclockdata_staging in logdata.txt -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
    Invoke-CrowbankSql "exec ppopulate_clockdata"    
}

Function Read-GF {
    [CmdletBinding()]
    param()
    process {
        New-Start
        Set-Location C:/tmp

        [int]$mid = (Invoke-CrowbankSql "select isnull(max(fd_no), 0) id from tblformdata_19").id
        Write-Verbose $mid
        $sql = "select * from crwbnk_gf_entry where form_id=19 and id > $mid;"
        Write-Verbose $sql

        ssh -p 88 crowbank@cp165172.hpdns.net "mysql -pCrowbank454 -u crowbank_petadmin --database crowbank_wp301 --execute '$sql'" > v.csv

        Get-Content v.csv | Select-Object -Skip 1 | Foreach-Object -Process {
            $a = $_.Split("`t")
            [int]$id = $a[0]
            [int]$form = $a[1]
            $created = $a[3]
            $updated = $a[4]
            "{0}`t{1}`t{2}`t{3}`n" -f $id, $form, $created, $updated
        } | Out-File -FilePath v.txt -NoNewLine -Encoding ASCII

        Invoke-CrowbankSql -Database mysql "truncate table crwbnk_gf_entry"

        bcp crwbnk_gf_entry in v.txt -U PA -P petadmin -S HP-SERVER\SqlExpress -d mysql -c -t "\t" -r "0x0a"

        $sql = "select * from crwbnk_gf_entry_meta where form_id=19 and entry_id > $mid;"
        Write-Verbose $sql

        ssh -p 88 crowbank@cp165172.hpdns.net "mysql -pCrowbank454 -u crowbank_petadmin --database crowbank_wp301 --execute '$sql'" > vm.csv

        Get-Content vm.csv | Select-Object -Skip 1 | Foreach-Object -Process {
            $a = $_.Split("`t")
            [int]$id = $a[0]
            [int]$form = $a[1]
            [int]$entry = $a[2]
            $key = $a[3]
            $value = $a[4]
            "{0}`t{1}`t{2}`t{3}`t{4}`n" -f $id, $form, $entry, $key, $value
        } | Out-File -FilePath vm.txt -NoNewLine -Encoding ASCII

        Invoke-CrowbankSql -Database mysql "truncate table crwbnk_gf_entry_meta"

        bcp crwbnk_gf_entry_meta in vm.txt -U PA -P petadmin -S HP-SERVER\SqlExpress -d mysql -c -t "\t" -r "0x0a"

        Invoke-CrowbankSql "exec pimport_gf_entry"
    }
}

Set-Alias -Name hp -Value Enter-HPServer
Set-Alias -Name log -Value New-LogEntry
Set-Alias -Name error -Value New-Error
Set-Alias -Name warn -Value New-Warning
Set-Alias -Name gb -Value Get-Booking
Set-Alias -Name gcu -Value Get-Customer
Set-Alias -Name gcust -Value Get-Customer
Set-Alias -Name gpet -Value Get-Pet
Set-Alias -Name gvet -Value Get-Vet
Set-Alias -Name gbreed -Value Get-Breed
Set-Alias -Name confirm -Value Send-Confirmation
Set-Alias -Name pay -Value New-Payment
Set-Alias -Name sql -Value Invoke-CrowbankSql
Set-Alias -Name plink -Value New-PayLink
Set-Alias -Name vet -Value New-VetBill
Set-Alias -Name book -Value New-Booking
Set-Alias -Name help -Value Get-Help
Set-Alias -Name gr -Value Get-Request
Set-Alias -Name im -Value Import-Module
Set-Alias -Name ic -Value Install-Crowbank

$m = 'Petadmin-Module'
Export-ModuleMember -Function * -Alias * -Variable m