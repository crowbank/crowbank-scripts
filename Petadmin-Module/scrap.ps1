$bookings | Where-Object {
    $b = $_
    if ($null -eq $item) {
        $true
    }
    else {
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $b.bk_no -eq $item
            }
            "System.String" {
                $b.cust_surname -Match $item -or
                $b.cust_addr1 -Match $item -or
                $b.cust_addr3 -Match $item -or
                $b.cust_postcode -Match $item -or
                $b.cust_email -Match $item -or
                $b.cust_email2 -Match $item
            }
            "Customer" {
                $b.bk_cust_no -eq $item.CustomerNo
            }
            "Pet" {
                $b.bk_petnos.Split(",").Contains($item.PetNo)
            }
        }
    }
} | Where-Object {
    ($Time -Match "P" -and $_.bk_end_date -le (Get-Date)) -or
    ($Time -Match "C" -and $_.bk_end_date -ge (Get-Date) -and $_.bk_start_date -le (Get-Date)) -or
    ($Time -Match "F" -and $_.bk_start_date -ge (Get-Date))
} | Where-Object {
    ($Status -Match "B" -and $_.bk_status -eq ' ') -or
    $Status -Match $_.bk_status
} | Where-Object {
    $null -eq $On -or ($_.bk_start_date -le $On -and $_.bk_end_date -ge $On )
} | ForEach-Object { [Booking]::new($_) }
