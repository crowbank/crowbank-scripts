Class Customer {
    [Int16] $no
    [String] $forename
    [String] $surname
    [String] $email
    [String] $telno_home
    [String] $telno_mobile
    [String] $telno_mobile2
    [String] $email2
    [String] $addr1
    [String] $addr3
    [String] $postcode
    [datetime] $create_date
    [bool] $nodeposit
}

Class Breed {
    [Int16] $no
    [String] $desc
    [String] $spec
}

Class Pet {
    [Int16] $no
    [String] $name
    [Breed] $breed
}