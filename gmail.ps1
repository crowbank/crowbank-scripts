﻿function testfunc {
    param (
        [Parameter()]
        [int]$Count,
        [Parameter()]
        [string]$word,
        [Parameter(HelpMessage="Environment to use [default=PROD, unless Dev environment variable is set]")]
        [Alias("Env")]
        [ValidateSet("PROD", "DEV")]
        [string]$ProductionOrDevelopment = $Env:Prod
     )
     process {
        $MyInvocation | Get-Member
        $MyInvocation | Format-List -Property *
        $MyInvocation.ScriptName.Split('\')[-1]
     }
}

testfunc 3