﻿Set-Location 'C:\Users\the_y\Dropbox\Kennels\Medication Sheets'
$re = '(\d+) (\d\d\d\d-\d\d-\d\d)$'
Get-ChildItem | ForEach-Object {
    $file = $_.BaseName

    if ($file -match $re) {
        [int]$pet_no = $Matches[1]
        [datetime]$date = $Matches[2]

        $pet = Get-Pet $pet_no
        $bookings = $pet | Get-Booking -T P
        $this_booking = $bookings  | Where-Object { $_.StartDate -le $date -and $_.EndDate -ge $date }
        $bk_no = $this_booking.BookingNo

        if ($null -ne $bk_no) {
            $new_name = "$($pet.Name) $bk_no.pdf"
            Write-Verbose "Moving $file.pdf to renamed/$new_name"
            Move-Item "$file.pdf" "renamed/$new_name"
        }
    }
}