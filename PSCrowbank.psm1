Function Invoke-CrowbankSql {
    param(
        [Parameter(HelpMessage="SQL Command",
        Mandatory = $true,
        Position = 0)]
        [string] $Command,
        [Parameter(HelpMessage="Use Development Environment")]
        [Switch]
        [Alias("dev", "D")]
        $Development,
        [Parameter(HelpMessage="Database to use")]
        [string] $Database = "crowbank"
    )

    if ($Development) {
        $ServerInstance = "localhost\SQLEXPRESS"
    } else {
        $ServerInstance = "HP-SERVER\SQLEXPRESS"
    }
    $Username = "PA"
    $Password = "petadmin"

    Invoke-Sqlcmd -ServerInstance "$ServerInstance" -Database "$Database" -Username "$Username" -Password "$Password" "$Command"
}

Function Invoke-CrowbankMysql {
    param(
        [Parameter(HelpMessage="SQL Command",
        Mandatory = $true,
        Position = 0)]
        [string] $Command,
        [Parameter(HelpMessage="Use Development Environment")]
        [Switch]
        [Alias("dev", "D")]
        $Development,
        [Parameter(HelpMessage="Database to use")]
        [string] $Database = "crowbank_petadmin"
    )

    if ($Development) {
        $ServerInstance = "localhost"
    } else {
        $ServerInstance = "HP-SERVER"
    }

    $MYSQLPASSWORD = "Crowbank454"
    $MYSQLUSER = "crowbank"
    

}

function my ($cmd) {
    mysql "-p$MYSQLPASSWORD" "-u$MYSQLUSER" -e "$cmd" --skip-column-names -s 2> $null
}

Function ConvertTo-String ( $DateInput ) {
    If ([string]::IsNullOrWhiteSpace($DateInput)) {
        return "NULL"
    }

    [datetime]$date = $DateInput
    return "'$($date.ToString('yyyy-MM-dd'))'"
}

Function Read-Clockdata {
    param (
        [string]$InFile,
        [string]$OutFile
    )
    (Get-Content GLogData_.txt) | Select-Object -Skip 1 | ForEach-Object -Begin { $h = @{} } -Process {
        $a = $_.Split("`t")
        $d = $a[6].Substring(0, 10) -Replace "/", "-"
        $t = $a[6].Substring(12, 5)
        [int]$n = $a[2]
        $e = $a[3].Trim()
        $k = $e + $d
        $h.Item($k) += 1
        "{0}`t{1}`t{2}`t{3}`t{4}`n" -f $d, $t, $n, $e, $h.Item($k)
    } | Out-File -FilePath logdata.txt -NoNewline -Encoding ASCII
    # awk 'BEGIN {OFS = "\t"} NR > 1 {gsub("/", "-", $7); seq = ++a[$4 $7]; print $7, substr($8, 1, 5), $3+0, $4, seq}' > logdata.txt
    Invoke-CrowbankSql "truncate table tblclockdata_staging"
    bcp tblclockdata_staging in logdata.txt -U PA -P petadmin -S HP-SERVER\SqlExpress -c -t "\t" -r "0x0a"
    Invoke-CrowbankSql "exec ppopulate_clockdata"
}

Function Read-Date {
    param (
        [string]$Prompt,
        [datetime]$MinDate,
        [string]$StartValue,
        [Switch]$AllowNull
    )

    $date = New-Object DateTime

    do {
        if ([string]::IsNullOrWhiteSpace($StartValue)) {
            $response = Read-Host $Prompt
        } else {
            $response = $StartValue
        }

        $StartValue = ""
        if ([string]::IsNullOrWhiteSpace($response)) {
            if ($AllowNull) {
                return
            }
            Write-Host "A valid date must be entered."
            continue
        }

        if ($response -match "(\d+)/(\d+)") {
            $day = $Matches[1]
            $month = $Matches[2]
            $year = (Get-Date).Year
            $date = Get-Date -Year $year -Month $month -Day $day
            if ($null -ne $MinDate -and $date -lt $MinDate) {
                $date = $date.AddYears(1)
            }
            return $date
        } elseif ([DateTime]::TryParse($response, [Ref]$date)) {
            if ($null -eq $MinDate -or $date -ge $MinDate) {
                return $date
            } else {
                Write-Host ("Date must be on or after {0}" -f $MinDate.ToString("dd/MM/yyyy"))
            }
        } else {
            Write-Host "Enter date in valid format such as yyyy-mm-dd, or simply dd/mm"
        }
    } while ($true)
}

Function Read-Amount {
    param (
        [Parameter(Mandatory=$true)]
        [string]$Prompt,
        [Switch]$AllowNull
    )

    [decimal]$amount = 0.0

    Do {
        $response = Read-Host $Prompt
        If ([string]::IsNullOrWhiteSpace($response)) {
            If ($AllowNull) {
                Return
            }
            Write-Host "A valid amount must be entered."
            Continue
        }
        If ([decimal]::TryParse($response, [ref]$amount)) {
            return $amount
        }
        Write-Host "Please enter an amount"
    } While ($true)
}
Function Test-Confirmation
{
    [CmdletBinding(SupportsShouldProcess)]
    Param(
        [Parameter(ValueFromPipeline=$true)]
        $item
    )
    process {
        if ($PSCmdlet.ShouldProcess("$item")) {
            Write-Host "Moving Ahead"
        } else {
            Write-Host "Ok, will stop"
        }
    }
}
Function Test-ValidDate
{
    param(
        [string]$string
    )

    $string -ne '' -and $string.GetType().FullName -ne 'DBNull'
}


class Employee
{
    [int]$EmployeeNo
    [string]$Surname
    [string]$Forename
    [string]$Nickname
    [string]$Addr1
    [string]$Addr3
    [string]$Postcode
    [string]$Mobile
    [string]$Emergency
    [string]$TelnoHome
    [string]$Email
    [string]$Facebook
    [string]$Rank
    [datetime]$StartDate
    [datetime]$EndDate
    [datetime]$DOB
    [decimal]$CurrentRate
    [bool]$IsCurrent
    [int]$Order
    
    Employee([System.Data.DataRow]$row) {
        $this.EmployeeNo = $row.emp_no
        $this.Surname = $row.emp_surname
        $this.Forename = $row.emp_forename
        $this.Nickname = $row.emp_nickname
        $this.Addr1 = $row.emp_addr1
        $this.Addr3 = $row.emp_addr3
        $this.Postcode = $row.emp_postcode
        $this.Mobile = $row.emp_telno_mobile
        $this.Emergency = $row.emp_telno_emergency
        $this.TelnoHome = $row.emp_telno_home
        $this.Email = $row.emp_email
        $this.Facebook = $row.emp_facebook
        $this.DOB = $row.emp_dob
        $this.IsCurrent = $row.emp_iscurrent
        If (![string]::IsNullOrWhiteSpace($row.emp_current_rate)) {
            $this.CurrentRate = $row.emp_current_rate
        }
        if ($row.emp_start_date -is [datetime]) {
            $this.StartDate = $row.emp_start_date
        }
        if ($row.emp_end_date -is [datetime]) {
            $this.EndDate = $row.emp_end_date
        }
        if ($row.emp_order -is [int]) {
            $this.Order = $row.emp_order
        } else {
            $this.Order = 0
        }
        
        $this.Rank = $row.emp_rank
    }

    [string] ToString() {
        return $this.Nickname
    }
}

Function New-Employee {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Nickname (must be unique)",
        Mandatory=$true)]
        [string]$Nickname,
        [Parameter(HelpMessage="First Name",
        Mandatory=$true)]
        [string]$Forename,
        [Parameter(HelpMessage="Last Name",
        Mandatory=$true)]
        [string]$Surname,
        [Parameter(HelpMessage="Email Address")]
        [string]$Email,
        [Parameter(HelpMessage="Facebook Address")]
        [string]$Facebook,
        [Parameter(HelpMessage="Street Address")]
        [Alias("Street", "Addr1")]
        [string]$StreetAddress,
        [Parameter(HelpMessage="City")]
        [Alias("City", "Town", "Addr3")]
        [string]$CityAddress,
        [Parameter(HelpMessage="Postcode")]
        [string]$Postcode,
        [Parameter(HelpMessage="Mobile Number")]
        [Alias("Telno_Mobile", "Telno")]
        [string]$Mobile,
        [Parameter(HelpMessage="Emergency Number")]
        [Alias("Telno_Emergency")]
        [string]$Emergency,
        [Parameter(HelpMessage="Home Number")]
        [Alias("Telno_Home", "HomeNumber")]
        [string]$TelnoHome,
        [Parameter(HelpMessage="Start Date")]
        [Alias("Start")]
        [datetime]$StartDate,
        [Parameter(HelpMessage="National Insurance Number")]
        [string]$NI,
        [Parameter(HelpMessage="Date of Birth")]
        [datetime]$DOB,
        [Parameter(HelpMessage="Rank of Employee")]
        [ValidateSet("Manager", "Shift Leader", "Kennel Assistant", "Dog Walker", "Volunteer",
        "M", "S", "K", "D", "V")]
        [string]$Rank,
        [Parameter(HelpMessage="Current Rate")]
        [Alias("Rate")]
        [decimal]$CurrentRate
    )

    $sql = "Select count(*) c from tblemployee where emp_nickname = '$Nickname'"
    $c = (Invoke-CrowbankSql $sql).c

    If ($c -gt 0) {
        throw "Employee nicknamed $Nickname already exists"
    }

    If ([string]::IsNullOrWhiteSpace($Email)) { $Email = Read-Host "Email" }
    If ([string]::IsNullOrWhiteSpace($Facebook)) { $Facebook = Read-Host "Facebook Address" }
    If ([string]::IsNullOrWhiteSpace($StreetAddress)) { $StreetAddress = Read-Host "Street Address" }
    If ([string]::IsNullOrWhiteSpace($CityAddress)) { $CityAddress = Read-Host "City" }
    If ([string]::IsNullOrWhiteSpace($Postcode)) { $Postcode = Read-Host "Postcode" }
    If ([string]::IsNullOrWhiteSpace($Mobile)) { $Mobile = Read-Host "Mobile Number" }
    If ([string]::IsNullOrWhiteSpace($Emergency)) { $Emergency = Read-Host "Emergency Number" }
    If ([string]::IsNullOrWhiteSpace($TelnoHome)) { $TelnoHome = Read-Host "Home Number" }
    If ([string]::IsNullOrWhiteSpace($StartDate)) { $StartDate = Read-Date "Start Date" }
    If ([string]::IsNullOrWhiteSpace($NI)) { $NI = Read-Host "National Insurance Number" }
    If ([string]::IsNullOrWhiteSpace($DOB)) { $DOB = Read-Date -AllowNull "Date of Birth" }
    $RankNumber = 'NULL'
    Do {
        If ([string]::IsNullOrWhiteSpace($Rank)) {
            $Rank = Read-Host "Rank ([M]anager, [S]hift Leader, [K]ennel Assistant, [D]og Walker or [V]olunteer)"
            If ([string]::IsNullOrWhiteSpace($Rank)) {
                Continue
            }
        }
        $sql = "Select min(er_no) er_no from tblemployeerank where er_desc like '$Rank%'"
        Write-Verbose $sql
        $RankNumber = (Invoke-CrowbankSql $sql).er_no
        If ($null -eq $RankNumber) {
            Write-Host "Unknown Rank"
            $Rank = ''
        }
    } While ( $null -eq $RankNumber)

    If ($CurrentRate -eq 0.0) { $CurrentRate = Read-Amount -AllowNull "Current Rate" }
    If ($StartDate -gt (Get-Date)) {
        $IsCurrent = 0
    } else {
        $IsCurrent = 1
    }

    If ($CurrentRate -gt 0.0) {
        $sql = "Execute pupdate_employee_rate '$Nickname', $(ConvertTo-String($StartDate)), $CurrentRate"
        Write-Verbose $sql
        Invoke-CrowbankSql $sql
        $RateString = $CurrentRate.ToString()
    } else {
        $RateString = "NULL"
    }

    $sql = @"
Insert into tblemployee
(emp_nickname, emp_forename, emp_surname, emp_email, emp_facebook, emp_addr1, emp_addr3, emp_postcode,
emp_telno_mobile, emp_telno_emergency, emp_telno_home, emp_start_date, emp_ni, emp_dob, emp_current_rate,
emp_iscurrent, emp_rank_no) values (
    '$Nickname', '$Forename', '$Surname', '$Email', '$Facebook', '$StreetAddress', '$CityAddress', '$Postcode',
    '$Mobile', '$Emergency', '$TelnoHome', $(ConvertTo-String($StartDate)), '$NI', $(ConvertTo-String($DOB)),
    $RateString, $IsCurrent, $RankNumber)
"@

    Write-Verbose $sql
    Invoke-CrowbankSql $sql

$sql = @"
Exec pupdate_employee_rate '$Nickname', $(ConvertTo-String($StartDate)), $RateString
"@

    Write-Verbose $sql
    Invoke-CrowbankSql $sql

    $sql = "Select emp_no from tblemployee where emp_nickname = '$Nickname'"
    Write-Verbose $sql
    [int]$Number = (Invoke-CrowbankSql $sql).emp_no

    Get-Employee -EmployeeNo $Number -All
}
Function Get-Employee {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Employee Number")]
        [Alias("No", "Number", "EmpNumber")]
        [ValidateRange(1,999999)]
        [int] $EmployeeNo,
        [Parameter(HelpMessage="Get all employees, including non-current ones")]
        [Switch]
        [Alias("A")]
        $All
    )
    begin {
        $cmd = "Select * from vwemployee"
        $emps = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $emps | Where-Object {
                $emp = $_
                if (-not $All -and $emp.emp_iscurrent -eq 0) { $false }
                elseif ($EmployeeNo -gt 0) { $emp.emp_no -eq $EmployeeNo }
                elseif ($null -eq $item) { $true }
                else {
                    Switch($item.GetType().FullName ) {
                        "System.String" {
                            $emp.emp_forename -Match $item -or
                            $emp.emp_surname -Match $item -or
                            $emp.emp_nickname -Match $item -or
                            $emp.emp_addr1 -Match $item -or
                            $emp.emp_addr3 -Match $item -or
                            $emp.emp_postcode -Match $item -or
                            $emp.emp_telno_mobile -Match $item -or
                            $emp.emp_email -Match $item -or
                            $emp.emp_facebook -Match $item -or
                            $emp.emp_telno_emergency -Match $item -or
                            $emp.emp_telno_home -Match $item -or
                            $emp.emp_rank -Match $item
                        }
                        "System.Int32" {
                            $emp.emp_no -eq $item    
                        }
                    }
                }    
            } | ForEach-Object { [Employee]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property Nickname -Unique
    }
}


class Vet
{
    [int]$VetNo
    [string]$PracticeName
    [string]$Addr1
    [string]$Addr3
    [string]$Postcode
    [string]$Telno1
    [string]$Email
    [string]$Website

    Vet([System.Data.DataRow]$row) {
        $this.VetNo = $row.vet_no
        $this.PracticeName = $row.vet_practice_name
        $this.Addr1 = $row.vet_addr1
        $this.Addr3 = $row.vet_addr3
        $this.Postcode = $row.vet_postcode
        $this.Telno1 = $row.vet_telno_1
        $this.Email = $row.vet_email
        $this.Website = $row.vet_website
    }

    [string]ToString() {
        return ("{0}: {1}" -f $this.VetNo, $this.PracticeName)
    }
}

Function Get-Vet {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Vet Number")]
        [Alias("No", "Number", "VetNumber")]
        [ValidateRange(1,999999)]
        [int] $VetNo
    )
    begin {
        $cmd = "Select * from vwvet_mysql"
        $vets = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $vets | Where-Object {
                $vet = $_
                if ($VetNo -gt 0) { $vet.vet_no -eq $VetNo }
                elseif ($null -eq $item) { $true }
                else {
                    Switch($item.GetType().FullName ) {
                        "System.String" {
                            $vet.vet_practice_name -Match $item -or
                            $vet.vet_addr1 -Match $item -or
                            $vet.vet_addr3 -Match $item -or
                            $vet.vet_postcode -Match $item -or
                            $vet.vet_telno_1 -Match $item -or
                            $vet.vet_email -Match $item -or
                            $vet.vet_website -Match $item    
                        }
                        "System.Int32" {
                            $vet.vet_no -eq $item    
                        }
                        "Pet" {
                            $vet.vet_no -eq $item.VetNo    
                        }
                    }
                }    
            } | ForEach-Object { [Vet]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property VetNo -Unique
    }
}

class Breed
{
    [int]$BreedNo
    [string]$Species
    [string]$Description
    [string]$ShortDescription
    [string]$BillCategory

    Breed([System.Data.DataRow]$row) {
        $this.BreedNo = $row.breed_no
        if ($row.breed_spec_no -eq 1 ) {
            $this.Species = 'Dog'
        } else {
            $this.Species = 'Cat'
        }
        $this.Description = $row.breed_desc
        $this.ShortDescription = $row.breed_shortdesc
        $this.BillCategory = switch($row.breed_billcat_no) {
            -1 { "Misc" }
            3 { "Dog large" }
            5 { "Dog medium" }
            7 { "Dog small" }
            11 { "Cat" }
        }
    }

    [string]ToString(){
        return ("{0}: {1}" -f $this.BreedNo, $this.Description)
    }
}

Function Get-Breed {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Breed Number")]
        [Alias("No", "Number", "BreedNumber")]
        [ValidateRange(1,999999)]
        [int] $BreedNo
    )
    begin {
        $out = @()
        $cmd = "Select * from vwbreed_mysql"
        $breeds = Invoke-CrowbankSql $cmd
    }
    process {
        $out += (
            $breeds | Where-Object {
                $b = $_
                if ($BreedNo -gt 0) { $_.breed_no -eq $BreedNo}
                elseif ($null -eq $item ) { $true }
                else { 
                    Switch ( $item.GetType().FullName ) {
                        "System.Int32" { $b.breed_no -eq $item }
                        "System.String" {
                            $b.breed_desc -Match $item -or
                            $b.breed_shortdesc -Match $item
                        }
                        "Pet" {
                            $b.breed_no -eq $item.BreedNo
                        }
                    }
                }
            } | ForEach-Object { [Breed]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property BreedNo -Unique
    }
}

class Customer
{
    [int]$CustomerNo
    [string]$Title
    [string]$Surname
    [string]$Forename
    [string]$Addr1
    [string]$Addr3
    [string]$Postcode
    [string]$TelnoHome
    [string]$TelnoMobile
    [string]$TelnoMobile2
    [string]$Email
    [string]$Email2
    [bool]$NoDeposit

    [int[]]$PetNos
    [string[]]$PetNames

    Customer([System.Data.DataRow]$row) {
        $this.CustomerNo = $row.cust_no
        $this.Title = $row.cust_title
        $this.Surname = $row.cust_surname
        $this.Forename = $row.cust_forename
        $this.Addr1 = $row.cust_addr1
        $this.Addr3 = $row.cust_addr3
        $this.Postcode = $row.cust_postcode
        $this.TelnoHome = $row.cust_telno_home
        $this.TelnoMobile = $row.cust_telno_mobile
        $this.TelnoMobile2 = $row.cust_telno_mobile2
        $this.Email = $row.cust_email
        $this.Email2 = $row.cust_email2
        $this.NoDeposit = $row.cust_nodeposit
        $this.PetNos = $row.cust_petnos.Split(",")
        $this.PetNames = $row.cust_petnames.Split(",")
    }

    [string]ToString(){
        $full_name = $this.Surname
        if ($this.Forename -ne '') {
            $full_name = $this.Forename + ' ' + $this.Surname
        }
        if ($this.Title -ne '') {
            $full_name = $this.Title + ' ' + $full_name
        }
        return ("{0}: {1} [{2}]" -f $this.CustomerNo, $full_name, $this.Email)
    }

}

Function Get-Customer {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Search by customer number, name, address, phone number, etc.", ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Customer Number")]
        [Alias("No", "Number", "CustomerNumber")]
        [ValidateRange(1,999999)]
        [int] $CustomerNo
    )
    begin {
        $cmd = "Select * from vwcustomer_ps"
        $customers = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $customers | Where-Object {
                $c = $_
                if ( $CustomerNo -gt 0 ) {
                    Write-Verbose "Getting based on customer number $CustomerNo"
                    $CustomerNo -eq $_.cust_no 
                }
                elseif ( $null -eq $item ) {
                    Write-Verbose "Getting all customers"
                    $true
                } else {
                    Switch ($item.GetType().FullName) {
                        "System.Int32" {
                            Write-Verbose "Getting based on integer customer number $item"
                            $item -eq $c.cust_no
                        }
                        "System.String" {
                            Write-Verbose "Searching for string $item"
                            $c.cust_surname -Match $item -or
                            $c.cust_forename -Match $item -or
                            $c.cust_addr1 -Match $item -or
                            $c.cust_addr3 -Match $item -or
                            $c.cust_postcode -Match $item -or
                            $c.cust_email -Match $item -or
                            $c.cust_email2 -Match $item            
                        }
                        "Pet" {
                            Write-Verbose "Getting customer for pet $item"
                            $c.cust_no -eq $item.CustomerNo 
                        }
                        "Booking" {
                            Write-Verbose "Getting customer for booking $item"
                            $c.cust_no -eq $item.CustomerNo
                        }
                    }
                }
            } | ForEach-Object { [Customer]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property CustomerNo -Unique
    }
}


class Pet
{
    [int]$PetNo
    [int]$CustomerNo
    [string]$Surname
    [string]$Name
    [string]$Species
    [int]$BreedNo
    [string]$BreedDescription
    [datetime]$DOB
    [string]$Warning
    [char]$Sex
    [char]$Neutered
    [int]$VetNo
    [string]$VaccinationStatus
    [datetime]$VaccinationDate
    [bool]$Deceased
    [datetime]$KCDate
    [string]$Notes
    [string]$VaccinationPath

    [string]ToString(){
        return ("{0}: {1} {2} ({3})" -f $this.PetNo, $this.Name, $this.Surname, $this.BreedDescription)
    }

    Pet([System.Data.DataRow]$row) {
        $this.PetNo = $row.pet_no
        $this.CustomerNo = $row.pet_cust_no
        $this.Surname = $row.cust_surname

        $this.Name = $row.pet_name
        $this.BreedNo = $row.pet_breed_no
        $this.BreedDescription = $row.breed_shortdesc

        if (Test-ValidDate $row.pet_dob) { $this.DOB = $row.pet_dob }
        $this.Warning = $row.pet_warning
        $this.Sex = $row.pet_sex
        $this.Neutered = $row.pet_neutered
        $this.VetNo = $row.pet_vet_no

        $this.VaccinationStatus = $row.pet_vacc_status
        if (Test-ValidDate $row.pet_vacc_date ) { $this.VaccinationDate = $row.pet_vacc_date }
        $this.Deceased = $row.pet_deceased
        if (Test-ValidDate $row.pet_kc_date ) { $this.KCDate = $row.pet_kc_date }
        $this.Notes = $row.pet_notes
        $this.VaccinationPath = $row.vacc_path
    }
}

Function Get-Pet {
    [CmdletBinding()]
    param (
        [Parameter(HelpMessage="Search by pet number, name and breed", ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Pet Number")]
        [Alias("No", "Number", "PetNumber")]
        [ValidateRange(1,999999)]
        [int] $PetNo
    )
    begin {
        $cmd = "Select * from vwpet_mysql join vwcustomer_mysql on cust_no = pet_cust_no join vwbreed_mysql on breed_no = pet_breed_no"
        $pets = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        $out += (
            $pets | Where-Object {
                $p = $_
                if ($PetNo -gt 0) {
                    $p.pet_no -eq $PetNo 
                }
                elseif ($null -eq $item) {
                    $true
                } else {
                    Switch ($item.GetType().FullName ) {
                        "System.Int32" {
                            $item -eq $p.pet_no
                        }
                        "System.String" {
                            $p.pet_name -Match $item -or
                            $p.cust_surname -Match $item -or
                            $p.cust_addr1 -Match $item -or
                            $p.cust_addr3 -Match $item -or
                            $p.cust_postcode -Match $item -or
                            $p.cust_email -Match $item -or
                            $p.cust_email2 -Match $item -or
                            $p.breed_desc -Match $item -or
                            $p.breed_shortdesc -Match $item
                        }
                        "Customer" {
                            $item.PetNos.Contains($p.pet_no)
                        }
                        "Booking" {
                            $item.PetNos.Contains($p.pet_no)
                        }
                        "Breed" {
                            $p.breed_no -eq $item.BreedNo 
                        }
                    }
                }
            } | ForEach-Object { [Pet]::new($_) }
        )
    }
    end {
        $out | Sort-Object -Property PetNo -Unique
    }
}

class Booking
{
    [int]$BookingNo
    [int]$CustomerNo
    [datetime]$StartDate
    [datetime]$EndDate
    [decimal]$GrossAmount
    [decimal]$PaidAmount
    [string]$Notes
    [string]$Memo
    [char]$Status
    [datetime]$CreateDate
    [bool]$Deluxe
    [string]$PetNames
    [string]$CustomerSurname

    [int[]]$PetNos

    Booking([System.Data.DataRow]$row) {
        $this.BookingNo = $row.bk_no
        $this.CustomerNo = $row.bk_cust_no
        $this.CustomerSurname = $row.cust_surname

        $this.StartDate = $row.bk_start_date + $row.bk_start_time
        $this.EndDate = $row.bk_end_date + $row.bk_end_time
        $this.GrossAmount = $row.bk_gross_amt
        $this.PaidAmount = $row.bk_paid_amt
        $this.Notes = $row.bk_notes
        $this.Memo = $row.bk_memo
        $this.Status = $row.bk_status
        $this.CreateDate = $row.bk_create_date
        $this.Deluxe = $row.bk_deluxe

        $this.PetNos = $row.bk_petnos.Split(",")
        $this.PetNames = $row.bk_petnames
    }

    [string]ToString(){
        return ("{0}: {1} [{2} - {3}]" -f $this.BookingNo, $this.PetNames, $this.StartDate, $this.EndDate)
    }

    [void]SendConfirmation() {
        python C:\Crowbank\confirm.py -booking $($this.BookingNo) -review
    }
}

Function Send-Confirmation {
    [CmdletBinding(SupportsShouldProcess=$true,
    ConfirmImpact="High")]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item
    )
    process {
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $booking = Get-Booking -BookingNo $item
            }
            "Booking" {
                $booking = $item
            }
        }
        $booking.SendConfirmation()
    }
}

function Get-Booking {
    [CmdletBinding()]
    param (
        [Parameter(ValueFromPipeline=$true)]
        $item,
        [Parameter(HelpMessage="Booking Number")]
        [Alias("No", "Number", "BookingNumber")]
        [int] $BookingNo,
        [Parameter(HelpMessage="Time: P (past), C (current), F (future) or any combination, e.g. CF for current or future")]
        [Alias("T")]
        [string] $Time = "CF",
        [Parameter(HelpMessage="Booking Status: B (regular), V (confirmed), C (cancelled), P (provisional), S (standby), N (no-show)")]
        [string] $Status = "BV",
        [Parameter(HelpMessage="Force refresh of staging table; use just after making changes; will make retrieval slower")]
        [Switch]
        $Force,
        [Parameter(HelpMessage="Bookings active on the date")]
        [datetime] $On
    )
    begin {
        if ($force) {
            Write-Verbose "Running pmaster"
            Invoke-CrowbankSql "Execute pmaster"
        }
        if ($BookingNo -gt 0) {
            $abort = $true
            $cmd = "Select * from ps_booking join ps_customer on cust_no = bk_cust_no where bk_no = $BookingNo"
            $row = Invoke-CrowbankSql $cmd
            return [Booking]::new($row)
        }
        $cmd = "Select * from ps_booking join ps_customer on cust_no = bk_cust_no"
        $abort = $false
        $bookings = Invoke-CrowbankSql $cmd
        $out = @()
    }
    process {
        if (-not $abort) {
            $out += (
                $bookings | Where-Object {
                    $b = $_
                    if ($null -eq $item) {
                        $true
                    }
                    else {
                        Switch($item.GetType().FullName) {
                            "System.Int32" {
                                $b.bk_no -eq $item
                            }
                            "System.String" {
                                $b.cust_surname -Match $item -or
                                $b.cust_addr1 -Match $item -or
                                $b.cust_addr3 -Match $item -or
                                $b.cust_postcode -Match $item -or
                                $b.cust_email -Match $item -or
                                $b.cust_email2 -Match $item
                            }
                            "Customer" {
                                $b.bk_cust_no -eq $item.CustomerNo
                            }
                            "Pet" {
                                $b.bk_petnos.Split(",").Contains($item.PetNo)
                            }
                        }
                    }
                } | Where-Object {
                    ($Time -Match "P" -and $_.bk_end_date -le (Get-Date)) -or
                    ($Time -Match "C" -and $_.bk_end_date -ge (Get-Date) -and $_.bk_start_date -le (Get-Date)) -or
                    ($Time -Match "F" -and $_.bk_start_date -ge (Get-Date))
                } | Where-Object {
                    ($Status -Match "B" -and $_.bk_status -eq ' ') -or
                    $Status -Match $_.bk_status
                } | Where-Object {
                    $null -eq $On -or ($_.bk_start_date -le $On -and $_.bk_end_date -ge $On )
                } | ForEach-Object { [Booking]::new($_) }
            )
        }
    }
    end {
        $out | Sort-Object -Property BookingNo -Unique
    }
}

Function New-Booking {
    [CmdletBinding(SupportsShouldProcess = $true,
    ConfirmImpact = 'High')]
    param (
        [Parameter(HelpMessage="Customer for which booking is to be made",
        ValueFromPipeline=$true, Position=0, Mandatory=$true)]
        $item,
        [Parameter(HelpMessage="Start Date")]
        [datetime]$StartDate,
        [Parameter(HelpMessage="End Date")]
        [datetime]$EndDate,
        [Parameter(HelpMessage="Start Time [am or pm]")]
        [ValidateSet("am", "pm")]
        [string]$StartTime,
        [Parameter(HelpMessage="End Time [am or pm]")]
        [ValidateSet("am", "pm")]
        [string]$EndTime,
        [Parameter(HelpMessage="Pets: list names or use 'all' (default)")]
        [string[]]$Pets = "all",
        [Parameter(HelpMessage="Is Deluxe Booking")]
        [switch]
        $Deluxe,
        [Parameter(HelpMessage="Comment or booking note")]
        [string]$Comment,
        [Parameter(HelpMessage="Status ([B]ooking (default), or [V]erified, [P]rovisional, [S]tandby")]
        [ValidateSet("B", "V", "P", "S")]
        [string]$Status = "B"
    )

    process {
        If ($item -is [Customer]) {
            $Customer = $item
        } else {
            $Customer = Get-Customer $item
            If ($Customer.Count -eq 0) {
                Write-Warning "Unable to find customer"
                Return
            } elseif ($Customer.Count -gt 1) {
                Write-Warning "Search item $item matches more than one customer"
                Return
            }
        }

        If ($Pets -eq "All") {
            $Pets = Get-Pet $Customer | Where-Object { -not $_.Deceased }
        } else {
            $Pets = $Pets | Get-Pet | Where-Object { $_.CustomerNo -eq $Customer.CustomerNo -and -not $_.Deceased }
        }

        $PetNames = ($Pets | ForEach-Object { $_.Name }) -Join ", "
        $PetNumbers = ($pets | ForEach-Object { $_.PetNo }) -Join ","

        $StartDate = Read-Date "Start Date" -MinDate (Get-Date) -StartValue $StartDate

        If ($StartDate.DayOfWeek -eq 'Sunday') {
            $StartTime = "am"
        } elseif ($null -eq $StartTime) {
            Do {
                $StartTime = Read-Host "Start Time ([am] (default) or [pm])"
                If ($null -eq $StartTime) {
                    $StartTime = "am"
                }
                If ($StartTime -eq "am" -or $StartTime -eq "pm") {
                    Continue
                }
            } While ($true)
        }

        $EndDate = Read-Date "End Date" -MinDate $StartDate -StartValue $EndDate

        If ($EndDate.DayOfWeek -eq "Sunday") {
            $EndTime = "am"
        } elseif ($null -eq $EndTime) {
            Do {
                $EndTime = Read-Host "End Time ([am] (default) or [pm])"
                If ($null -eq $EndTime) {
                    $EndTime = "am"
                }
                If ($EndTime -eq "am" -or $EndTime -eq "pm") {
                    Continue
                }
            } While ($true)
        }
        
        If ($Deluxe) {
            $IsDeluxe = 1
        } else {
            $IsDeluxe = 0
        }

        $msg =
@"
Create booking (status $Status) for customer $($Customer.Surname) #$($Customer.CustomerNo)
for $PetNames from $($StartDate.ToString('dd/MM/yyyy')) ($StartTime) to $(EndDate.ToString('dd/MM/yyyy')) ($EndTime)
"@
        if ($pscmdlet.ShouldProcess($msg)) {
            $sql =
@"
Execute pcreate_booking $($Customer.CustomerNo), $(ConvertTo-String($StartDate)), $(ConvertTo-String($EndDate)),
'$StartTime', '$EndTime', $PetNumbers, NULL, @Status, $IsDeluxe, '$Comment'
"@
            Write-Verbose $sql
            Invoke-CrowbankSql $sql
        }
    }
}
Function New-Payment {
    [CmdletBinding(SupportsShouldProcess = $true,
    ConfirmImpact = 'High')]
    param (
        [Parameter(HelpMessage="Provide a booking or a booking number",
        ValueFromPipeline=$true, Position=0, Mandatory=$true)]
        $item,
        [Parameter(HelpMessage="Payment Method (one of Pay By Link, Worldpay Terminal, Cash or Bank Xfer)")]
        [ValidateSet("Pay by Link", "Worldpay Terminal", "Cash", "Bank Xfer")]
        [Alias("PT", "Type")]
        [string]$PaymentType = "Pay by Link",
        [Parameter(HelpMessage="Payment Date (defaults to today's date)")]
        [Alias("Date", "D", "On")]
        [datetime]$PaymentDate,
        [Parameter(HelpMessage="Use yesterday as payment date")]
        [Switch]
        [Alias("Y")]
        $Yesterday,
        [Switch]
        $Force,
        [Parameter(HelpMessage="Payment Amount (defaults to requested deposit)")]
        [Alias("A")]
        [decimal]$Amount
    )
    begin {
        if ($null -eq $PaymentDate) {
            $PaymentDate = (Get-Date)
            if ($Yesterday) {
                $PaymentDate = $PaymentDate.AddDays(-1)
            }
        }
    }
    process {
        Switch($item.GetType().FullName) {
            "System.Int32" {
                $bk_no = $item
                $booking = Get-Booking -BookingNo $bk_no
            }
            "Booking" {
                $bk_no = $item.BookingNo
                $booking = $item
            }
        }

        if ($null -eq $booking) {
            Write-Error "Could not load booking #$bk_no"
            return
        }

        if ($booking.PaidAmount -gt 0.0 -and -not $Force) {
            Write-Host "A payment has already been made. To register another, used -Force"
            return
        }

        if ($null -eq $Amount -or $Amount -eq 0.0) {
            $Amount = (Invoke-CrowbankSql "Select * from tbldepositrequest where dr_bk_no = $bk_no").dr_amount
            if ($null -eq $Amount -or $Amount -eq 0.0) {
                Write-Error "Unable to find an amount for booking #$bk_no"
                return
            }
        }

        $msg = "Payment of £$Amount for $booking"
        if ($pscmdlet.ShouldProcess($msg)) {
            $cmd = "Execute ppayment $bk_no, $Amount, '$PaymentType', '$PaymentDate'"
            Write-Verbose "About to run $cmd"
            Invoke-CrowbankSql $cmd
        }
    }
}

Set-Alias -Name gb -Value Get-Booking
Set-Alias -Name gcu -Value Get-Customer
Set-Alias -Name gcust -Value Get-Customer
Set-Alias -Name gpet -Value Get-Pet
Set-Alias -Name gvet -Value Get-Vet
Set-Alias -Name gbreed -Value Get-Breed
Set-Alias -Name conf -Value Send-Confirmation
Set-Alias -Name pay -Value New-Payment
Set-Alias -Name sql -Value Invoke-CrowbankSql

Export-ModuleMember -Function * -Alias *