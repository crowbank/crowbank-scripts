﻿. C:\Crowbank\crowbank-functions.ps1

$tables = ("my_petinventory", "my_hrtask", "my_employeetask", "my_lastmsg", "my_billcategory", "my_booking", "my_bookingitem", "my_breed",
"my_customer", "my_pet", "my_species", "my_vet", "my_availability", "my_runavailability","my_timesheet", "my_message", "my_employeewage", "my_employee",
"my_task", "my_lasttransfer","my_inventory")

$TmpDir = "C:\tmp"

Push-Location $TmpDir

sqlcmd_cmd "select * from mysql..my_lasttransfer"
sqlcmd_cmd "SET ANSI_WARNINGS OFF; Execute crowbank..pToMySQL;"

foreach ($table in $tables) {
    Invoke-Expression -Command "bcp mysql..$table out $table.csv -c -S localhost\SQLEXPRESS -U $SQLCMDUSER -P $SQLCMDPASSWORD -o bcp.out -r ""\n"""
    Set-Variable -Name l -Value (Get-Content "$table.csv" | Measure-Object -Lines).Lines
    mysql_cmd "truncate table crowbank_staging.$table;"
    Invoke-Expression -Command "mysqlimport --lines-terminated-by=""\r\n"" -s -L -p$MYSQLPASSWORD -u$MYSQLUSER crowbank_staging $table.csv" 2> $null
    Set-Variable -Name c -Value (mysql_cmd "select count(*) from crowbank_staging.$table;")
    If ($c -ne $l) {
        Write-Host "Error: " $table "file has " $l "rows. DB table has " $c
    } Else {
        Remove-Item "$table.csv"
    }
}

sqlcmd_cmd "select * from mysql..my_lasttransfer"
mysql_cmd "select * from crowbank_staging.my_lasttransfer;"
Pop-Location