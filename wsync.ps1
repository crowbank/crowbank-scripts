. C:\Crowbank\crowbank-functions.ps1

Push-Location $TMP_DRIVE

# Transfer messages from MySQL to SQL Server
# Transferred tables are crowbank_wordpress.crwnk_messages and crowbank_wordpress.crwbnk_msgmeta

my "Call crowbank_wordpress.PrepareMessages"

$tables = ("crwbnk_messages", "crwbnk_msgmeta")

foreach ($table in $tables) {
    my "select * from crowbank_staging.$table" > "$table.csv"
    sql "truncate table mysql..$table"
    bcp_in "mysql..$table" "$TMP_DRIVE\$table.csv"
}

sql "Execute crowbank..pmessage_process"


$tables = ("my_petinventory", "my_hrtask", "my_employeetask", "my_lastmsg", "my_billcategory", "my_booking", "my_bookingitem", "my_breed",
"my_customer", "my_pet", "my_species", "my_vet", "my_availability", "my_runavailability","my_timesheet", "my_message", "my_employeewage", "my_employee",
"my_task", "my_lasttransfer","my_inventory")

sql "select * from mysql..my_lasttransfer"
sql "SET ANSI_WARNINGS OFF; Execute crowbank..pToMySQL;"

foreach ($table in $tables) {
    $file = "$table.csv"
    bcp_out "mysql..$table" "$file"
    Set-Variable -Name l -Value (Get-Content "$file" | Measure-Object -Line).Lines
    my "truncate table crowbank_staging.$table;"
    Invoke-Expression -Command "mysqlimport --lines-terminated-by=""\r\n"" -s -L -p$MYSQLPASSWORD -u$MYSQLUSER crowbank_staging $file" 2> $null
    Set-Variable -Name c -Value (my "select count(*) from crowbank_staging.$table;")
    If ($c -ne $l) {
        Write-Error -Messasge  "Error: $table file has $l rows. DB table has $c."
    } Else {
        Remove-Item "$file"
    }
}

my "Call crowbank_wordpress.MessagesProcessed"
my "Call crowbank_petadmin.ForceRefreshFromStaging();"

Pop-Location
